
use parse::{parse_escape_sequence, parse_symbol};
use ast::nodes::StringLiteral;
use ast::keywords::EscapeSequence;
use itertools::Either;
use util::{collect_string_vec, continue_if};
use failure::Error;
use errors::parse::primitives::string::*;

// Grabs either the next escape sequence, or the next run of characters until the end
// of the string or another escape sequence.
named!(parse_string_content<&str, Either<String, EscapeSequence>, Error>,
	parse_fail!(
		ErrParseStringContent,
		alt!(
			// Either parse an escape sequence...
			map!(
				do_parse!(
					escape: parse_escape_sequence >>
				
					// The SkipWhitespace escape excludes all following whitespace.
					cast_fail!(cond!(
						escape == EscapeSequence::SkipWhitespace,
						take_while_s!(|c: char| c.is_whitespace())
					)) >>
					(escape)
				),
				|escape| Either::Right(escape)
			) |

			// ...or until the next escape sequence or the end of the string.
			map!(
				recognize!(
					many1!(not_escape_or_quote)
				),
				|s| Either::Left(s.to_string())
			)
		)
	)
);

// This is necessary as a sub-macro of parse_string_content because the not! macro doesn't
// consume its input.
//
// If there is no escape or quote, this will consume 1 char.
named!(not_escape_or_quote<&str, &str, Error>,
	parse_fail!(
		ErrNotEscapeOrQuote,
		complete!(do_parse!(
			escape: fixed_opt!(parse_escape_sequence) >>
			quote: cast_fail!(opt!(tag_s!("\""))) >>
			// Returns an error if the condition is false.
			c: complete!(cond_reduce!(
				escape == Option::None && quote == Option::None,
				cast_fail!(take_s!(1))
			)) >>
			(c)
		))
	)
);

named!(parse_normal_string<&str, StringLiteral, Error>, 
	parse_fail!(
		ErrParseNormalString,
		map!(
			do_parse!(
				cast_fail!(tag_s!("\"")) >>
				content: many0!(complete!(parse_string_content)) >>
				cast_fail!(tag_s!("\"")) >>
				(content)
			),
			|content| StringLiteral::new(content)
		)
	)
);

// See section 3.1 for more info about long brackets.
named!(pub parse_long_bracket_string<&str, StringLiteral, Error>,
	parse_fail!(
		ErrParseLongBracketString,
		map!(
			do_parse!(
				symbol: parse_symbol >>
				call!(continue_if, symbol.is_open_long_bracket()) >>
				depth: expr_opt!(symbol.get_bracket_depth()) >>
		
				// If there's a newline immediately after the open bracket, discard it.
				cast_fail!(opt!(tag_s!("\n"))) >>

				text: many_till!(
					cast_fail!(take_s!(1)),
					do_parse!(
						symbol: parse_symbol >>
						expr_opt!(if symbol.get_bracket_depth() == Some(depth) {
							Some(())
						} else {
							// Return an Error
							None
						}) >>
						(symbol)
					)
				) >>
				(text)
			),
			|(real_text, _)| StringLiteral::new_bracketed(collect_string_vec(&real_text))
		)
	)
);

named!(pub parse_string<&str, StringLiteral, Error>,
	parse_fail!(
		ErrParseString,
		alt!(
			parse_normal_string |
			parse_long_bracket_string
		)
	)
);

#[cfg(test)]
mod test {
	
	use super::{StringLiteral, EscapeSequence};
	use nom::IResult::Done;
	use super::Either::{Left, Right};
	

	#[test]
	fn test_parse_string_content() {
		use super::parse_string_content;

		// Escape codes
		assert_that!("\\n", parse_string_content).is(Done("", Right(EscapeSequence::Newline)));
		assert_that!("\\045", parse_string_content).is(Done("", Right(EscapeSequence::Byte(45))));
		assert_that!("\\b \\a", parse_string_content).is(Done(" \\a", Right(EscapeSequence::Backspace)));
		assert_that!("\\", parse_string_content).is(Done("", Right(EscapeSequence::EmptyEscape)));
		assert_that!("\\yyyy", parse_string_content)
			.is(Done("yyy", Right(EscapeSequence::InvalidEscape('y'))));

		assert_that!("\\u{cafebabe}hello", parse_string_content)
			.is(Done("hello", Right(EscapeSequence::Unicode(0xcafebabe))));

		// Plain strings
		assert_that!("Hello!", parse_string_content).is(Done("", Left("Hello!".to_string())));
		assert_that!("    spaces work too", parse_string_content)
			.is(Done("", Left("    spaces work too".to_string())));
		assert_that!("newlines \n are parsed", parse_string_content)
			.is(Done("", Left("newlines \n are parsed".to_string())));

		// Strings with escape codes
		assert_that!("\\t\\n test", parse_string_content).is(Done("\\n test", Right(EscapeSequence::Tab)));
		assert_that!("Hello\\!", parse_string_content).is(Done("\\!", Left("Hello".to_string())));
		assert_that!("Hello\\!j", parse_string_content).is(Done("\\!j", Left("Hello".to_string())));
		assert_that!("This is a string.\\n And another.", parse_string_content)
			.is(Done("\\n And another.", Left("This is a string.".to_string())));


		assert_is_true!("", parse_string_content => is_incomplete);
	}

	#[test]
	fn test_not_escape_or_quote() {
		use super::not_escape_or_quote;

		assert_that!("Normal string", not_escape_or_quote).is(Done("ormal string", "N"));
		assert_that!("First bit \\n is okay", not_escape_or_quote).is(Done("irst bit \\n is okay", "F"));

		assert_is_true!("\" quote!", not_escape_or_quote => is_err);
		assert_is_true!("\\a", not_escape_or_quote => is_err);
		assert_is_true!("", not_escape_or_quote => is_err);
		assert_is_true!("\"", not_escape_or_quote => is_err);
	}

	#[test]
	fn test_parse_normal_string() {
		use super::parse_normal_string;

		let mut all_tests = Vec::new();

		let test1_text = "\"Hello, there!\\nFancy meeting you here!\" After the quotes!";
		let test1_extra_text = " After the quotes!";
		let test1_array = vec![
			Left("Hello, there!".to_string()),
			Right(EscapeSequence::Newline),
			Left("Fancy meeting you here!".to_string())
		];
		all_tests.push((test1_text, test1_extra_text, test1_array));

		let test2_text = "\"\\tTest, test, test,\n gotta test 'em all...\\n\\y\"no love for Bell \\b";
		let test2_extra_text = "no love for Bell \\b";
		let test2_array = vec![
			Right(EscapeSequence::Tab),
			Left("Test, test, test,\n gotta test 'em all...".to_string()),
			Right(EscapeSequence::Newline),
			Right(EscapeSequence::InvalidEscape('y'))
		];
		all_tests.push((test2_text, test2_extra_text, test2_array));

		for (text, extra_text, array) in all_tests {
			let copy_extra_text = extra_text.clone();
			let copy_array = array.clone();
			let literal = StringLiteral::new(copy_array);
			assert_that!(&text, parse_normal_string).is(Done(copy_extra_text, literal));
		}
	}

	#[test]
	fn test_parse_bracket_string() {
		use super::parse_long_bracket_string;

		assert_that!("[[test]]", parse_long_bracket_string)
			.is(Done("", StringLiteral::new_bracketed("test".to_string())));

		assert_that!("[=[depth test]=]", parse_long_bracket_string)
			.is(Done("", StringLiteral::new_bracketed("depth test".to_string())));

		assert_that!("[=[Tricky stuff [[test]]]=]]", parse_long_bracket_string)
			.is(Done("]",StringLiteral::new_bracketed("Tricky stuff [[test]]".to_string())));

		assert_that!("[===[Checking \n newlines ]====] and \\t escapes]===]v]===]", parse_long_bracket_string)
			.is(Done("v]===]", StringLiteral::new_bracketed(
				"Checking \n newlines ]====] and \\t escapes".to_string())));

		assert_that!("[[]]Empty", parse_long_bracket_string)
			.is(Done("Empty", StringLiteral::new_bracketed("".to_string())));
		
		assert_is_true!("[=[]", parse_long_bracket_string => is_incomplete);
		assert_is_true!("[=[test]=", parse_long_bracket_string => is_incomplete);
		assert_is_true!("[=[test]==]", parse_long_bracket_string => is_incomplete);

		assert_is_true!("]=]", parse_long_bracket_string => is_err);
		assert_is_true!("[", parse_long_bracket_string => is_err);
		assert_is_true!("hello", parse_long_bracket_string => is_err);
		assert_is_true!("\"hello, again\"", parse_long_bracket_string => is_err);
		assert_is_true!("", parse_long_bracket_string => is_err);
	}

	#[test]
	fn test_parse_string() {
		use super::parse_string;

		assert_that!("\"Hello\" World", parse_string)
			.is(Done(" World", StringLiteral::new(vec![Left("Hello".to_string())])));

		assert_that!("[=[testing\\n]] much test]=] wow", parse_string)
			.is(Done(" wow", StringLiteral::new_bracketed("testing\\n]] much test".to_string())));
	}
	
}