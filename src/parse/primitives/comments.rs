
use parse::parse_symbol;
use util::continue_if;
use parse::primitives::string::parse_long_bracket_string;
use ast::keywords::Symbol;
use failure::Error;
use errors::parse::primitives::comments::*;

named!(pub parse_comment<&str, (), Error>, 
	parse_fail!(
		ErrParseComment,
		do_parse!(
			symbol: parse_symbol >>
			call!(continue_if, symbol == Symbol::Comment) >>
			alt!(
				map!(parse_long_bracket_string, |_| "") |
				cast_fail!(take_while_s!(|c: char| c != '\n'))
			) >>
			()
		)
	)
);

#[cfg(test)]
mod test {
	
	use nom::IResult::Done;
	
	
	#[test]
	fn test_parse_comment() {
		use super::parse_comment;

		assert_that!("-- comment", parse_comment).is(Done("", ()));
		assert_that!("--stuff \nmore stuff", parse_comment).is(Done("\nmore stuff", ()));
		assert_that!("--[=[ looooong comment \n still going \\t and going \n done]=]", parse_comment)
			.is(Done("", ()));
		assert_that!("--[[stuff]] more stuff", parse_comment).is(Done(" more stuff", ()));

		assert_is_true!("--[=[ whatever]]", parse_comment => is_incomplete);
		assert_is_true!("-", parse_comment => is_err);
		assert_is_true!("", parse_comment => is_err);
		assert_is_true!("Whatever -- then comment", parse_comment => is_err);

	}
}