
use ast::keywords::Keyword;
use util::{take_c, fail_if};
use failure::Error;
use errors::parse::primitives::keywords::*;


named!(pub parse_keyword<&str, Keyword, Error>,
	parse_fail!(
		ErrParseKeyword,
		do_parse!(
			keyword: cast_fail!(alt!(
				// ElseIf is special because of the Else keyword.
				complete!(map!(tag_s!("elseif"), |_| Keyword::ElseIf)) |

				map!(tag_s!("true"), |_| Keyword::True) |
				map!(tag_s!("false"), |_| Keyword::False) |
				map!(tag_s!("nil"), |_| Keyword::Nil) |

				map!(tag_s!("and"), |_| Keyword::And) |
				map!(tag_s!("or"), |_| Keyword::Or) |
				map!(tag_s!("not"), |_| Keyword::Not) |

				map!(tag_s!("if"), |_| Keyword::If) |
				map!(tag_s!("then"), |_| Keyword::Then) |
				map!(tag_s!("else"), |_| Keyword::Else) |
				map!(tag_s!("end"), |_| Keyword::End) |

				map!(tag_s!("break"), |_| Keyword::Break) |
				map!(tag_s!("goto"), |_| Keyword::Goto) |

				map!(tag_s!("do"), |_| Keyword::Do) |
				map!(tag_s!("while"), |_| Keyword::While) |
				map!(tag_s!("until"), |_| Keyword::Until) |
				map!(tag_s!("for"), |_| Keyword::For) |
				map!(tag_s!("repeat"), |_| Keyword::Repeat) |
				map!(tag_s!("in"), |_| Keyword::In) |

				map!(tag_s!("function"), |_| Keyword::Function) |
				map!(tag_s!("local"), |_| Keyword::Local) |
				map!(tag_s!("return"), |_| Keyword::Return)
			)) >>
			alt_complete!(
				do_parse!(
					c: peek!(take_c) >>
					call!(fail_if, c.is_alphanumeric()) >>
					()
				) |
				cast_fail!(map!(eof!(), |_| ()))
			) >>
			(keyword)
		)
	)
);


#[cfg(test)]
mod test {
	
	use super::{Keyword, parse_keyword};
	use nom::IResult::Done;

	

	#[test]
	fn test_parse_keyword() {
		assert_that!("true", parse_keyword).is( Done("", Keyword::True));
		assert_that!("false", parse_keyword).is(Done("", Keyword::False));
		assert_that!("nil", parse_keyword).is(Done("", Keyword::Nil));

		assert_that!("and", parse_keyword).is(Done("", Keyword::And));
		assert_that!("or", parse_keyword).is(Done("", Keyword::Or));
		assert_that!("not", parse_keyword).is(Done("", Keyword::Not));
		
		assert_that!("if", parse_keyword).is(Done("", Keyword::If));
		assert_that!("then", parse_keyword).is(Done("", Keyword::Then));
		assert_that!("elseif", parse_keyword).is(Done("", Keyword::ElseIf));
		assert_that!("end", parse_keyword).is(Done("", Keyword::End));
		
		assert_that!("break ", parse_keyword).is(Done(" ", Keyword::Break));
		assert_that!("goto", parse_keyword).is(Done("", Keyword::Goto));

		assert_that!("do", parse_keyword).is(Done("", Keyword::Do));
		assert_that!("while", parse_keyword).is(Done("", Keyword::While));
		assert_that!("until", parse_keyword).is(Done("", Keyword::Until));
		assert_that!("for", parse_keyword).is(Done("", Keyword::For));
		assert_that!("repeat ", parse_keyword).is(Done(" ", Keyword::Repeat));
		assert_that!("in", parse_keyword).is(Done("", Keyword::In));
		
		assert_that!("function", parse_keyword).is(Done("", Keyword::Function));
		assert_that!("local", parse_keyword).is(Done("", Keyword::Local));
		assert_that!("return ", parse_keyword).is(Done(" ", Keyword::Return));


		assert_that!("goto   testing", parse_keyword).is(Done("   testing", Keyword::Goto));
		assert_that!("if then else", parse_keyword).is(Done(" then else", Keyword::If));
		assert_that!("for()", parse_keyword).is(Done("()", Keyword::For));

		assert_is_true!("b", parse_keyword => is_incomplete);
		assert_is_true!("whil", parse_keyword => is_incomplete);
		assert_is_true!("", parse_keyword => is_incomplete);

		assert_is_true!("whatever", parse_keyword => is_err);
		assert_is_true!("blah if blah", parse_keyword => is_err);
		assert_is_true!("Return", parse_keyword => is_err);
		assert_is_true!("z", parse_keyword => is_err);
		assert_is_true!("DO", parse_keyword => is_err);
		assert_is_true!("endstuff", parse_keyword => is_err);
		assert_is_true!("else1", parse_keyword => is_err);
	}
}
