
use parse::{parse_symbol, parse_keyword};
use ast::keywords::{Keyword, Symbol};
use util::{continue_if, many0_whitespace, many1_whitespace};
use parse::variables::parse_name;
use failure::Error;
use errors::parse::primitives::labels::*;

named!(pub parse_label<&str, String, Error>,
	parse_fail!(
		ErrParseLabel,
		do_parse!(
			start: parse_symbol >>
			call!(continue_if, start == Symbol::DoubleColon) >>
			many0_whitespace >>
			name: parse_name >>
			many0_whitespace >>
			end: parse_symbol >>
			call!(continue_if, end == Symbol::DoubleColon) >>
			(name.to_string())
		)
	)
);

named!(pub parse_goto<&str, String, Error>,
	parse_fail!(
		ErrParseGoto,
		do_parse!(
			keyword: parse_keyword >>
			call!(continue_if, keyword == Keyword::Goto) >>
			many1_whitespace >>
			name: parse_name >>
			(name.to_string())
		)
	)
);

#[cfg(test)]
mod test {
	
	use nom::IResult::Done;
	

	#[test]
	fn test_parse_label() {
		use super::parse_label;

		assert_that!("::test::", parse_label).is(Done("", "test".to_string()));
		assert_that!(":: anotherTest ::", parse_label).is(Done("", "anotherTest".to_string()));
		assert_that!("::\nextreme_example:: stuff", parse_label).is(Done(" stuff", "extreme_example".to_string()));
		assert_that!("::label42::57", parse_label).is(Done("57", "label42".to_string()));
		assert_that!("::ONE::TWO::", parse_label).is(Done("TWO::", "ONE".to_string()));
		assert_that!("::whatever:::", parse_label).is(Done(":", "whatever".to_string()));
		
		assert_is_true!("::", parse_label => is_incomplete);
		
		assert_is_true!("", parse_label => is_err);
		assert_is_true!("test", parse_label => is_err);
		assert_is_true!(":: ::", parse_label => is_err);
		assert_is_true!(":::again::", parse_label => is_err);
		assert_is_true!(":", parse_label => is_err);
		assert_is_true!(":: something something ::", parse_label => is_err);
		assert_is_true!(":: 44done ::", parse_label => is_err);
		assert_is_true!("::::", parse_label => is_err);
		assert_is_true!(" :: test ::", parse_label => is_err);
	}

	#[test]
	fn test_parse_goto() {
		use super::parse_goto;

		assert_that!("goto whatever", parse_goto).is(Done("", "whatever".to_string()));
		assert_that!("goto\nanotherLine", parse_goto).is(Done("", "anotherLine".to_string()));
		assert_that!("goto two words", parse_goto).is(Done(" words", "two".to_string()));
		assert_that!("goto whitespace ", parse_goto).is(Done(" ", "whitespace".to_string()));

		assert_is_true!("goto", parse_goto => is_incomplete);
		assert_is_true!("got", parse_goto => is_incomplete);
		assert_is_true!("g", parse_goto => is_incomplete);
		assert_is_true!("goto    ", parse_goto => is_incomplete);

		assert_is_true!("if stuff", parse_goto => is_err);
		assert_is_true!("goto 42something", parse_goto => is_err);
		assert_is_true!(" goto label", parse_goto => is_err);
	}

}