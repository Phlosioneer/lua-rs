
use nom::IResult;
use ast::keywords::Symbol;
use std::convert::TryFrom;
use util::continue_if;
use failure::Error;
use errors::parse::primitives::symbols::*;

named!(parse_simple_symbol<&str, Symbol, Error>,
	parse_fail!(
		ErrParseSimpleSymbol,
		cast_fail!(alt_complete!(
			// Three character ones first.
			map!(tag_s!("..."), |_| Symbol::Ellipsis) |
			map!(tag_s!("==="), |_| Symbol::ErrTripleEqual) |

			// Now two character ones.
			map!(tag_s!("<<"), |_| Symbol::DoubleLess) |
			map!(tag_s!(">>"), |_| Symbol::DoubleGreater) |
			map!(tag_s!("//"), |_| Symbol::DoubleForward) |
			map!(tag_s!("=="), |_| Symbol::DoubleEqual) |
			map!(tag_s!("~="), |_| Symbol::NotEqual) |
			map!(tag_s!("<="), |_| Symbol::LessEqual) |
			map!(tag_s!(">="), |_| Symbol::GreaterEqual) |
			map!(tag_s!("::"), |_| Symbol::DoubleColon) |
			map!(tag_s!(".."), |_| Symbol::DotDot) |
			map!(tag_s!("--"), |_| Symbol::Comment) |
			map!(tag_s!("!="), |_| Symbol::ErrNotEqual) |
			map!(tag_s!("&&"), |_| Symbol::ErrAnd) |
			map!(tag_s!("||"), |_| Symbol::ErrOr) |
			map!(tag_s!("*="), |_| Symbol::ErrMultEqual) |
			map!(tag_s!("/="), |_| Symbol::ErrDivEqual) |
			map!(tag_s!("+="), |_| Symbol::ErrAddEqual) |
			map!(tag_s!("-="), |_| Symbol::ErrSubEqual) |
			map!(tag_s!("++"), |_| Symbol::ErrIncrement) |

			// Now one character ones.
			map!(tag_s!("+"), |_| Symbol::Plus) |
			map!(tag_s!("-"), |_| Symbol::Minus) |
			map!(tag_s!("*"), |_| Symbol::Star) |
			map!(tag_s!("/"), |_| Symbol::ForwardSlash) |
			map!(tag_s!("%"), |_| Symbol::Percent) |
			map!(tag_s!("^"), |_| Symbol::Carot) |
			map!(tag_s!("#"), |_| Symbol::HashTag) |
			map!(tag_s!("&"), |_| Symbol::Amp) |
			map!(tag_s!("~"), |_| Symbol::Tilde) |
			map!(tag_s!("|"), |_| Symbol::Bar) |
			map!(tag_s!("<"), |_| Symbol::Less) |
			map!(tag_s!(">"), |_| Symbol::Greater) |
			map!(tag_s!("="), |_| Symbol::Equal) |
			map!(tag_s!("("), |_| Symbol::OpenParen) |
			map!(tag_s!(")"), |_| Symbol::CloseParen) |
			map!(tag_s!("{"), |_| Symbol::OpenCurl) |
			map!(tag_s!("}"), |_| Symbol::CloseCurl) |
			map!(tag_s!("["), |_| Symbol::OpenSquare) |
			map!(tag_s!("]"), |_| Symbol::CloseSquare) |
			map!(tag_s!(";"), |_| Symbol::SemiColon) |
			map!(tag_s!(":"), |_| Symbol::Colon) |
			map!(tag_s!(","), |_| Symbol::Comma) |
			map!(tag_s!("."), |_| Symbol::Period) |
			map!(tag_s!("!"), |_| Symbol::ErrNot)
		))
	)
);

named!(parse_open_long_bracket<&str, Symbol, Error>,
	parse_fail!(
		ErrParseOpenLongBracket,
		cast_fail!(map!(
			do_parse!(
				tag_s!("[") >>
				level: take_while!(|c| c == '=') >>
				tag_s!("[") >>
				(level)
			),
			|s| match u8::try_from(s.len()) {
				Ok(depth) => Symbol::OpenLongBracket(depth),
				Err(_) => Symbol::OpenTooLongBracket(format!("[{}[", s))
			}
		))
	)
);

named!(parse_close_long_bracket<&str, Symbol, Error>,
	parse_fail!(
		ErrParseCloseLongBracket,
		cast_fail!(map!(
			do_parse!(
				tag_s!("]") >>
				level: take_while!(|c| c == '=') >>
				tag_s!("]") >>
				(level)
			),
			|s| match u8::try_from(s.len()) {
				Ok(depth) => Symbol::CloseLongBracket(depth),
				Err(_) => Symbol::CloseTooLongBracket(format!("]{}]", s))
			}
		))
	)
);

named!(pub parse_symbol<&str, Symbol, Error>,
	parse_fail!(
		ErrParseSymbol,
		alt_complete!(
			parse_open_long_bracket |
			parse_close_long_bracket |
			parse_simple_symbol
		)
	)
);

// This will return an error if the parsed symbol is not the allowed symbol.
// TODO: Test this.
pub fn parse_exact_symbol<'a>(i: &'a str, expected: Symbol) -> IResult<&'a str, Symbol, Error> {
	parse_exact_symbols(i, &vec![expected])
}

// This will return an error if the parsed symbol is not one of the allowed symbols.
// TODO: Test this.
pub fn parse_exact_symbols<'a>(i: &'a str, expected: &Vec<Symbol>) -> IResult<&'a str, Symbol, Error> {
	parse_fail!(
		i, 
		ErrParseExactSymbols,
		do_parse!(
			actual: parse_symbol >>
			call!(continue_if, expected.contains(&actual)) >>
			(actual)
		)
	)
}

#[cfg(test)]
mod test {
	use ast::keywords::Symbol;
	use super::{parse_simple_symbol, parse_open_long_bracket, parse_close_long_bracket, parse_symbol};
	use nom::IResult::Done;
	

	#[test]
	fn test_parse_simple_symbol_single() {
		// Unambiguous
		assert_that!("%", parse_simple_symbol).is(Done("", Symbol::Percent));
		assert_that!("^", parse_simple_symbol).is(Done("", Symbol::Carot));
		assert_that!("#", parse_simple_symbol).is(Done("", Symbol::HashTag));
		assert_that!("(", parse_simple_symbol).is(Done("", Symbol::OpenParen));
		assert_that!(")", parse_simple_symbol).is(Done("", Symbol::CloseParen));
		assert_that!("{", parse_simple_symbol).is(Done("", Symbol::OpenCurl));
		assert_that!("}", parse_simple_symbol).is(Done("", Symbol::CloseCurl));
		assert_that!(";", parse_simple_symbol).is(Done("", Symbol::SemiColon));

		assert_that!("%%", parse_simple_symbol).is(Done("%", Symbol::Percent));
		assert_that!("()", parse_simple_symbol).is(Done(")", Symbol::OpenParen));
		assert_that!(";abc", parse_simple_symbol).is(Done("abc", Symbol::SemiColon));

		// Test that these do not return IResult::Incomplete.
		assert_that!("+", parse_simple_symbol).is(Done("", Symbol::Plus));
		assert_that!("-", parse_simple_symbol).is(Done("", Symbol::Minus));
		assert_that!("*", parse_simple_symbol).is(Done("", Symbol::Star));
		assert_that!("|", parse_simple_symbol).is(Done("", Symbol::Bar));
		assert_that!("/", parse_simple_symbol).is(Done("", Symbol::ForwardSlash));
		assert_that!("&", parse_simple_symbol).is(Done("", Symbol::Amp));
		assert_that!("~", parse_simple_symbol).is(Done("", Symbol::Tilde));
		assert_that!("<", parse_simple_symbol).is(Done("", Symbol::Less));
		assert_that!(">", parse_simple_symbol).is(Done("", Symbol::Greater));
		assert_that!("=", parse_simple_symbol).is(Done("", Symbol::Equal));
		assert_that!("[", parse_simple_symbol).is(Done("", Symbol::OpenSquare));
		assert_that!("]", parse_simple_symbol).is(Done("", Symbol::CloseSquare));
		assert_that!(":", parse_simple_symbol).is(Done("", Symbol::Colon));
		assert_that!(".", parse_simple_symbol).is(Done("", Symbol::Period));
		assert_that!("!", parse_simple_symbol).is(Done("", Symbol::ErrNot));
		
		assert_that!("+-", parse_simple_symbol).is(Done("-", Symbol::Plus));
		assert_that!("[A]", parse_simple_symbol).is(Done("A]", Symbol::OpenSquare));
		assert_that!("[=[", parse_simple_symbol).is(Done("=[", Symbol::OpenSquare));

		assert_is_true!("", parse_simple_symbol => is_err);
	}

	#[test]
	fn test_parse_simple_symbol_double() {
		assert_that!("<<", parse_simple_symbol).is(Done("", Symbol::DoubleLess));
		assert_that!(">>", parse_simple_symbol).is(Done("", Symbol::DoubleGreater));
		assert_that!("//", parse_simple_symbol).is(Done("", Symbol::DoubleForward));
		assert_that!("==", parse_simple_symbol).is(Done("", Symbol::DoubleEqual));
		assert_that!("~=", parse_simple_symbol).is(Done("", Symbol::NotEqual));
		assert_that!("<=", parse_simple_symbol).is(Done("", Symbol::LessEqual));
		assert_that!(">=", parse_simple_symbol).is(Done("", Symbol::GreaterEqual));
		assert_that!("::", parse_simple_symbol).is(Done("", Symbol::DoubleColon));
		assert_that!("..", parse_simple_symbol).is(Done("", Symbol::DotDot));
		assert_that!("--", parse_simple_symbol).is(Done("", Symbol::Comment));
		assert_that!("!=", parse_simple_symbol).is(Done("", Symbol::ErrNotEqual));
		assert_that!("&&", parse_simple_symbol).is(Done("", Symbol::ErrAnd));
		assert_that!("||", parse_simple_symbol).is(Done("", Symbol::ErrOr));
		assert_that!("*=", parse_simple_symbol).is(Done("", Symbol::ErrMultEqual));
		assert_that!("/=", parse_simple_symbol).is(Done("", Symbol::ErrDivEqual));
		assert_that!("+=", parse_simple_symbol).is(Done("", Symbol::ErrAddEqual));
		assert_that!("-=", parse_simple_symbol).is(Done("", Symbol::ErrSubEqual));
		assert_that!("++", parse_simple_symbol).is(Done("", Symbol::ErrIncrement));

		assert_that!(">>>", parse_simple_symbol).is(Done(">", Symbol::DoubleGreater));
		assert_that!("::<>", parse_simple_symbol).is(Done("<>", Symbol::DoubleColon));
		assert_that!("--[=[", parse_simple_symbol).is(Done("[=[", Symbol::Comment));
	}

	#[test]
	fn test_parse_simple_symbol_triple() {
		assert_that!("...", parse_simple_symbol).is(Done("", Symbol::Ellipsis));
		assert_that!("===", parse_simple_symbol).is(Done("", Symbol::ErrTripleEqual));

		assert_that!("....", parse_simple_symbol).is(Done(".", Symbol::Ellipsis));
	}

	#[test]
	fn test_open_long_bracket() {
		use self::Symbol::OpenLongBracket;

		assert_that!("[[", parse_open_long_bracket).is(Done("", OpenLongBracket(0)));
		assert_that!("[=[", parse_open_long_bracket).is(Done("", OpenLongBracket(1)));
		assert_that!("[==[", parse_open_long_bracket).is(Done("", OpenLongBracket(2)));
		assert_that!("[=[]=]", parse_open_long_bracket).is(Done("]=]", OpenLongBracket(1)));
		assert_that!("[===[abc", parse_open_long_bracket).is(Done("abc", OpenLongBracket(3)));

		assert_is_true!("[", parse_open_long_bracket => is_incomplete);
		assert_is_true!("[===", parse_open_long_bracket => is_incomplete);
		assert_is_true!("", parse_open_long_bracket => is_incomplete);

		assert_is_true!("[=a=[", parse_open_long_bracket => is_err);
		assert_is_true!("]=]", parse_open_long_bracket => is_err);
		assert_is_true!("=", parse_open_long_bracket => is_err);
		assert_is_true!("=[", parse_open_long_bracket => is_err);
	}

	#[test]
	fn test_close_long_bracket() {
		use self::Symbol::CloseLongBracket;
		
		assert_that!("]]", parse_close_long_bracket).is(Done("", CloseLongBracket(0)));
		assert_that!("]=]", parse_close_long_bracket).is(Done("", CloseLongBracket(1)));
		assert_that!("]==]", parse_close_long_bracket).is(Done("", CloseLongBracket(2)));
		assert_that!("]=][=[", parse_close_long_bracket).is(Done("[=[", CloseLongBracket(1)));
		assert_that!("]===]abc", parse_close_long_bracket).is(Done("abc", CloseLongBracket(3)));

		assert_is_true!("]", parse_close_long_bracket => is_incomplete);
		assert_is_true!("]===", parse_close_long_bracket => is_incomplete);
		assert_is_true!("", parse_close_long_bracket => is_incomplete);

		assert_is_true!("]=a=]", parse_close_long_bracket => is_err);
		assert_is_true!("[=[", parse_close_long_bracket => is_err);
		assert_is_true!("=]", parse_close_long_bracket => is_err);
	}

	#[test]
	fn test_way_too_long_bracket() {
		use self::Symbol::OpenTooLongBracket;
		use self::Symbol::CloseTooLongBracket;

		let depth: usize = (u8::max_value() as usize) + 1;
		let mut too_long = String::with_capacity(depth);
		for _ in 0..depth {
			too_long.push('=');
		}

		let open_too_long = format!("[{}[", &too_long);
		let close_too_long = format!("]{}]", &too_long);

		assert_that!(&open_too_long, parse_open_long_bracket).is(Done("", OpenTooLongBracket(open_too_long.clone())));
		assert_that!(&close_too_long, parse_close_long_bracket).is(Done("", CloseTooLongBracket(close_too_long.clone())));

		let mut open_too_long_extra = open_too_long.clone();
		let mut close_too_long_extra = close_too_long.clone();

		open_too_long_extra.push_str("and then some");
		close_too_long_extra.push_str("[==[");
		
		assert_that!(&open_too_long_extra, parse_open_long_bracket)
			.is(Done("and then some", OpenTooLongBracket(open_too_long)));

		assert_that!(&close_too_long_extra, parse_close_long_bracket)
			.is(Done("[==[", CloseTooLongBracket(close_too_long)));
	}

	#[test]
	fn test_parse_symbol() {
		assert_that!("* b", parse_symbol).is(Done(" b", Symbol::Star));
		assert_that!("[==[++", parse_symbol).is(Done("++", Symbol::OpenLongBracket(2)));
		assert_that!("--[=[", parse_symbol).is(Done("[=[", Symbol::Comment));
		assert_that!("+=", parse_symbol).is(Done("", Symbol::ErrAddEqual));
		assert_that!("///", parse_symbol).is(Done("/", Symbol::DoubleForward));
		assert_that!("]=a=]", parse_symbol).is(Done("=a=]", Symbol::CloseSquare));

		assert_is_true!("", parse_symbol => is_err);
		assert_is_true!("a", parse_symbol => is_err);
		assert_is_true!("\"", parse_symbol => is_err);

	}
}