
use ast::nodes::Number;
use failure::Error;
use errors::parse::primitives::decimal_numbers::*;

named!(recognize_int_number_dec<&str, String, Error>,
	parse_fail!(
		ErrRecognizeIntNumberDec,
		map!(
			do_parse!(
				number: cast_fail!(take_while1!(|c: char| c.is_digit(10))) >>
				(number)
			),
			|number| format!("{}", number)
		)
	)
);

// Note: This is a helper method for recognize_science_number_dec
named!(recognize_signed_int_number_dec<&str, String, Error>,
	parse_fail!(
		ErrRecognizeSignedIntNumberDec,
		map!(
			do_parse!(
				opt_sign: cast_fail!(opt!(
					alt!(
						map!(tag_s!("-"), |_| "-") |
						map!(tag_s!("+"), |_| "")
					)
				)) >>
				number: recognize_int_number_dec >>
				(opt_sign.unwrap_or("").to_string(), number)
			),
			|(sign, number)| format!("{}{}", sign, number)
		)
	)
);

named!(recognize_float_number_dec<&str, String, Error>,
	parse_fail!(
		ErrRecognizeFloatNumberDec,
		map!(
			do_parse!(
				int: recognize_int_number_dec >>
				cast_fail!(tag_s!(".")) >>
				decimal: cast_fail!(take_while1!(|c: char| c.is_digit(10))) >>
				(int, decimal)
			),
			|(int, decimal)| format!("{}.{}", int, decimal)
		)
	)
);

named!(recognize_science_number_dec<&str, String, Error>,
	parse_fail!(
		ErrRecognizeScienceNumberDec,
		map!(
			do_parse!(
				mantessa: alt!(
					recognize_float_number_dec |
					recognize_int_number_dec
				) >>
				cast_fail!(tag_no_case_s!("e")) >>
				// Note: According to the Lua spec, the exponent field is optional.
				// 0 is the default value.
				exponent: fixed_opt!(complete!(recognize_signed_int_number_dec)) >>
				(mantessa, exponent.unwrap_or("0".to_string()))
			),
			|(m, e)| format!("{}e{}", m, e)
		)
	)
);

named!(parse_int_number_dec<&str, i64, Error>,
	parse_fail!(
		ErrParseIntNumberDec,
		map!(
			complete!(recognize_int_number_dec),
			|s| s.parse::<i64>().unwrap()
		)
	)
);

named!(parse_float_number_dec<&str, f64, Error>,
	parse_fail!(
		ErrParseFloatNumberDec,
		map!(
			complete!(recognize_float_number_dec),
			|s| s.parse::<f64>().unwrap()
		)
	)
);

named!(parse_science_number_dec<&str, f64, Error>,
	parse_fail!(
		ErrParseScienceNumberDec,
		map!(
			complete!(recognize_science_number_dec),
			|s| s.parse::<f64>().unwrap()
		)
	)
);

// This does the actual work of parsing a number.
named!(pub parse_number_dec<&str, Number, Error>,
	parse_fail!(
		ErrParseNumberDec,
		alt_complete!(
			map!(parse_science_number_dec, |x| Number::Float(x)) |
			map!(parse_float_number_dec, |x| Number::Float(x)) |
			map!(parse_int_number_dec, |x| Number::Int(x))
		)
	)
);


#[cfg(test)]
mod test {

	use nom::IResult::Done;

	use super::parse_int_number_dec;
	use super::parse_float_number_dec;
	use super::parse_science_number_dec;
	use super::parse_number_dec;

	use super::recognize_int_number_dec;
	use super::recognize_float_number_dec;
	use super::recognize_science_number_dec;

	use ast::nodes::Number;
	

	#[test]
	fn test_recognize_int_number_dec() {
		assert_that!("4", recognize_int_number_dec).is(Done("", "4".to_string()));
		assert_that!("53b", recognize_int_number_dec).is(Done("b", "53".to_string()));
		assert_that!("53", recognize_int_number_dec).is(Done("", "53".to_string()));

		assert_is_true!("", recognize_int_number_dec => is_incomplete);

		assert_is_true!("-", recognize_int_number_dec => is_err);
		assert_is_true!("-78", recognize_int_number_dec => is_err);
		assert_is_true!("a", recognize_int_number_dec => is_err);
		assert_is_true!("+4", recognize_int_number_dec => is_err);
	}

	#[test]
	fn test_recognize_float_number_dec() {
		assert_that!("4.3", recognize_float_number_dec).is(Done("", "4.3".to_string()));
		assert_that!("3.54.8", recognize_float_number_dec).is(Done(".8", "3.54".to_string()));

		assert_is_true!("3", recognize_float_number_dec => is_incomplete);
		assert_is_true!("100.", recognize_float_number_dec => is_incomplete);
		assert_is_true!("", recognize_float_number_dec => is_incomplete);

		assert_is_true!("-", recognize_float_number_dec => is_err);
		assert_is_true!("-3.445", recognize_float_number_dec => is_err);
		assert_is_true!("+53.5", recognize_float_number_dec => is_err);
		assert_is_true!("a4.0", recognize_float_number_dec => is_err);
		assert_is_true!("4b", recognize_float_number_dec => is_err);
		assert_is_true!("43.f5", recognize_float_number_dec => is_err);
		assert_is_true!("--4.23", recognize_float_number_dec => is_err);
		assert_is_true!("-+4.34", recognize_float_number_dec => is_err);
		assert_is_true!("+-9.0", recognize_float_number_dec => is_err);
		assert_is_true!("abc", recognize_float_number_dec => is_err);
	}

	#[test]
	fn test_recognize_science_number_ints_dec() {
		assert_that!("3e0", recognize_science_number_dec).is(Done("", "3e0".to_string()));
		assert_that!("4e-5", recognize_science_number_dec).is(Done("", "4e-5".to_string()));
		assert_that!("10E0", recognize_science_number_dec).is(Done("", "10e0".to_string()));
		assert_that!("8e9ba45", recognize_science_number_dec).is(Done("ba45", "8e9".to_string()));
		assert_that!("8009e", recognize_science_number_dec).is(Done("", "8009e0".to_string()));
		assert_that!("44E", recognize_science_number_dec).is(Done("", "44e0".to_string()));
		assert_that!("2e-", recognize_science_number_dec).is(Done("-", "2e0".to_string()));

		assert_is_true!("3", recognize_science_number_dec => is_incomplete);
		assert_is_true!("", recognize_science_number_dec => is_incomplete);

		assert_is_true!("-4e5", recognize_science_number_dec => is_err);
		assert_is_true!("5b", recognize_science_number_dec => is_err);
		assert_is_true!("abc", recognize_science_number_dec => is_err);
		assert_is_true!("e4", recognize_science_number_dec => is_err);
	}

	#[test]
	fn test_recognize_science_number_floats_dec() {
		assert_that!("3.3e0", recognize_science_number_dec).is(Done("", "3.3e0".to_string()));
		assert_that!("4.01e-5", recognize_science_number_dec).is(Done("", "4.01e-5".to_string()));
		assert_that!("10.0E0", recognize_science_number_dec).is(Done("", "10.0e0".to_string()));
		assert_that!("8.3e9ba45", recognize_science_number_dec).is(Done("ba45", "8.3e9".to_string()));
		assert_that!("5.3E4.1", recognize_science_number_dec).is(Done(".1", "5.3e4".to_string()));
		assert_that!("44.011e2.4", recognize_science_number_dec).is(Done(".4", "44.011e2".to_string()));
		assert_that!("9.4e", recognize_science_number_dec).is(Done("", "9.4e0".to_string()));
		assert_that!("77.9e-", recognize_science_number_dec).is(Done("-", "77.9e0".to_string()));

		assert_is_true!("4.", recognize_science_number_dec => is_incomplete);
		
		assert_is_true!("4.e4", recognize_science_number_dec => is_err);
		assert_is_true!("-4.9e5", recognize_science_number_dec => is_err);
	}

	#[test]
	fn test_parse_int_number_dec() {
		assert_that!("5", parse_int_number_dec).is(Done("", 5));
		assert_that!("90", parse_int_number_dec).is(Done("", 90));
		assert_that!("4.01", parse_int_number_dec).is(Done(".01", 4));

		assert_is_true!("+7", parse_int_number_dec => is_err);
		assert_is_true!("-3", parse_int_number_dec => is_err);
		assert_is_true!("-", parse_int_number_dec => is_err);
		assert_is_true!("+", parse_int_number_dec => is_err);
		assert_is_true!("abc", parse_int_number_dec => is_err);
		assert_is_true!("", parse_int_number_dec => is_err);
	}

	#[test]
	fn test_parse_float_number_dec() {
		assert_that!("4.3", parse_float_number_dec).is(Done("", 4.3));
		assert_that!("7.00", parse_float_number_dec).is(Done("", 7.0));
		assert_that!("4.2e-4", parse_float_number_dec).is(Done("e-4", 4.2));
		assert_that!("00.001.4", parse_float_number_dec).is(Done(".4", 0.001));

		assert_is_true!("-8.223", parse_float_number_dec => is_err);
		assert_is_true!("1", parse_float_number_dec => is_err);
		assert_is_true!(".4", parse_float_number_dec => is_err);	// TODO: Check the lua spec about this case.
		assert_is_true!("-", parse_float_number_dec => is_err);
		assert_is_true!("", parse_float_number_dec => is_err);
	}

	#[test]
	fn test_parse_science_number_dec() {
		assert_that!("1e0", parse_science_number_dec).is(Done("", 1.0));
		assert_that!("1.455e5", parse_science_number_dec).is(Done("", 145500.0));
		assert_that!("4e-1", parse_science_number_dec).is(Done("", 0.4));
		assert_that!("4.3e4.2", parse_science_number_dec).is(Done(".2", 43000.0));
		assert_that!("00.000001E6edfg", parse_science_number_dec).is(Done("edfg", 1.0));
		assert_that!("3e", parse_science_number_dec).is(Done("", 3.0));
		assert_that!("5e.", parse_science_number_dec).is(Done(".", 5.0));

		assert_is_true!("-4e1", parse_science_number_dec => is_err);
		assert_is_true!("1", parse_science_number_dec => is_err);
		assert_is_true!("4.7", parse_science_number_dec => is_err);
		assert_is_true!("-", parse_science_number_dec => is_err);
		assert_is_true!("e5", parse_science_number_dec => is_err);
		assert_is_true!("edf", parse_science_number_dec => is_err);
		assert_is_true!("", parse_science_number_dec => is_err);
	}

	#[test]
	fn test_parse_number_dec() {
		assert_that!("50", parse_number_dec).is(Done("", Number::Int(50)));
		assert_that!("4.8", parse_number_dec).is(Done("", Number::Float(4.8)));
		assert_that!("4.8e1", parse_number_dec).is(Done("", Number::Float(48.0)));
		assert_that!("43e", parse_number_dec).is(Done("", Number::Float(43.0)));
		assert_that!("4.3e", parse_number_dec).is(Done("", Number::Float(4.3)));
		assert_that!("1b", parse_number_dec).is(Done("b", Number::Int(1)));
		assert_that!("432abc", parse_number_dec).is(Done("abc", Number::Int(432)));
		assert_that!("7.", parse_number_dec).is(Done(".", Number::Int(7)));
		assert_that!("4.3e-.2", parse_number_dec).is(Done("-.2", Number::Float(4.3)));

		assert_is_true!("+4e+1", parse_number_dec => is_err);
		assert_is_true!("-", parse_number_dec => is_err);
		assert_is_true!("e4", parse_number_dec => is_err);
		assert_is_true!("-.4", parse_number_dec => is_err);
		assert_is_true!("", parse_number_dec => is_err);
	}
}