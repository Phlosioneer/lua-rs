

// TODO: Make hex_number parsing module.
mod decimal_numbers;
mod string_escapes;
mod symbols;
mod string;
mod keywords;
mod comments;
mod labels;

pub use self::string_escapes::parse_escape_sequence;
pub use self::string::parse_string;
pub use self::keywords::parse_keyword;
pub use self::symbols::{parse_symbol, parse_exact_symbol, parse_exact_symbols};
pub use self::comments::parse_comment;
pub use self::labels::{parse_label, parse_goto};
pub use self::numbers::parse_number;


pub mod numbers {
	use super::decimal_numbers::parse_number_dec;
	//use self::hex_number::parse_number_hex;
	use ast::nodes::Number;
	use failure::Error;
	use errors::parse::primitives::numbers::*;

	named!(pub parse_number<&str, Number, Error>,
		parse_fail!(
			ErrParseNumber,
			alt!(
				parse_number_dec // | parse_number_hex
			)
		)
	);
}
