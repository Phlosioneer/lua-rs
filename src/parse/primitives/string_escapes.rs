

use std::convert::TryFrom;
use ast::keywords::{EscapeSequence, MissingBracket};
use nom::IResult;
use util::{take_c, continue_if};
use failure::Error;
use errors::parse::primitives::string_escapes::*;

// TODO: is \45 allowed? (sorthand for \045)
named!(parse_decimal_byte_escape<&str, EscapeSequence, Error>,
	parse_fail!(
		ErrParseDecimalByteEscape,
		map!(
			do_parse!(
				first: take_c >>
				call!(continue_if, first.is_digit(10)) >>
				second: take_c >>
				third: take_c >>
				(first, second, third)
			),
			|(first, second, third)| {
				let s = format!("{}{}{}", first, second, third);
				match s.parse::<u16>() {
					Ok(big) => match u8::try_from(big) {
							Ok(small) => EscapeSequence::Byte(small),
							Err(_) => EscapeSequence::ByteTooLarge(big)
						},
					Err(_) => EscapeSequence::InvalidDecimalByte(first, second, third)
				}
			}
		)
	)
);

named!(parse_hex_byte_escape<&str, EscapeSequence, Error>,
	parse_fail!(
		ErrParseHexByteEscape,
		map!(
			cast_fail!(do_parse!(
				tag_no_case_s!("x") >>
				digits: take!(2) >>
				(digits)
			)),
			|s| match u8::from_str_radix(s, 16) {
				Ok(i) => EscapeSequence::Byte(i),
				Err(_) => {
					let mut chars = s.chars();
					let first = chars.next().unwrap();
					let second = chars.next().unwrap();
					EscapeSequence::InvalidHexByte(first, second)
				}
			}
		)
	)
);

named!(match_hex<&str, &str, Error>, 
	parse_fail!(
		ErrMatchHex,
		cast_fail!(take_while!(|c: char| c.is_digit(16)))
	)
);

named!(parse_unicode_escape<&str, EscapeSequence, Error>,
	// This is split into four alt! cases because using opt! was causing
	// IResult::Incomplete if the last bracket was missing. This also makes
	// the mapped function simpler.
	parse_fail!(
		ErrParseUnicodeEscape,
		alt_complete!(
			// Start and end brackets.
			map!(
				cast_fail!(do_parse!(
					tag_no_case_s!("u") >>
					tag_s!("{") >>
					number: take_while!(|c: char| c.is_alphanumeric()) >>
					tag_s!("}") >>
					(number)
				)),
				|s| {
					if let IResult::Done("", _) = match_hex(s) {
						if s.len() > 0 {
							match u64::from_str_radix(s, 16) {
								Ok(i) => EscapeSequence::Unicode(i),
								Err(_) => EscapeSequence::UnicodeTooLarge(s.to_string())
							}
						} else {
							EscapeSequence::InvalidUnicode(s.to_string())
						}
					} else {
						EscapeSequence::InvalidUnicode(s.to_string())
					}
				}
			) |

			// Only end bracket is missing.
			map!(
				do_parse!(
					cast_fail!(tag_no_case_s!("u")) >>
					cast_fail!(tag_s!("{")) >>
					number: match_hex >>
					(number)
				),
				|s| EscapeSequence::UnicodeWithoutBrackets(
					s.to_string(), MissingBracket::End)
			) |

			// Only start bracket is missing.
			map!(
				do_parse!(
					cast_fail!(tag_no_case_s!("u")) >>
					number: match_hex >>
					cast_fail!(tag_s!("}")) >>
					(number)
				),
				|s| EscapeSequence::UnicodeWithoutBrackets(
					s.to_string(), MissingBracket::Start)
			) |

			// Both brackets are missing.
			map!(
				do_parse!(
					cast_fail!(tag_no_case_s!("u")) >>
					number: match_hex >>
					(number)
				),
				|s| EscapeSequence::UnicodeWithoutBrackets(
					s.to_string(), MissingBracket::Both)
			)
		)
	)
);

named!(pub parse_escape_sequence<&str, EscapeSequence, Error>,
	parse_fail!(
		ErrParseEscapeSequence,
		do_parse!(
			cast_fail!(tag_s!("\\")) >>
			ret: alt!(
				cast_fail!(map!(eof!(), |_| EscapeSequence::EmptyEscape)) |
				cast_fail!(map!(tag_no_case_s!("a"), |_| EscapeSequence::Bell)) |
				cast_fail!(map!(tag_no_case_s!("b"), |_| EscapeSequence::Backspace)) |
				cast_fail!(map!(tag_no_case_s!("f"), |_| EscapeSequence::FormFeed)) |
				cast_fail!(map!(tag_no_case_s!("n"), |_| EscapeSequence::Newline)) |
				cast_fail!(map!(tag_no_case_s!("r"), |_| EscapeSequence::Carriage)) |
				cast_fail!(map!(tag_no_case_s!("t"), |_| EscapeSequence::Tab)) |
				cast_fail!(map!(tag_no_case_s!("v"), |_| EscapeSequence::VerticalTab)) |
				cast_fail!(map!(tag_no_case_s!("z"), |_| EscapeSequence::SkipWhitespace)) |
				cast_fail!(map!(tag_s!("\\"), |_| EscapeSequence::BackSlash)) |
				cast_fail!(map!(tag_s!("\""), |_| EscapeSequence::DoubleQuote)) |
				cast_fail!(map!(tag_s!("\'"), |_| EscapeSequence::SingleQuote)) |
				cast_fail!(map!(tag_s!("\n"), |_| EscapeSequence::LineBreak)) |
				parse_decimal_byte_escape |
				parse_hex_byte_escape |
				parse_unicode_escape |
				map!(take_c, |c| EscapeSequence::InvalidEscape(c))
			) >>
			(ret)
		)
	)
);

#[cfg(test)]
mod test {
	use super::parse_escape_sequence;
	use super::parse_unicode_escape;
	use super::parse_decimal_byte_escape;
	use super::parse_hex_byte_escape;
	use ast::keywords::{EscapeSequence, MissingBracket};
	use nom::IResult::Done;
	

	#[test]
	fn test_simple_escape_sequence() {
		test_simple_escape_sequence_capital('a', EscapeSequence::Bell);
		test_simple_escape_sequence_capital('b', EscapeSequence::Backspace);
		test_simple_escape_sequence_capital('f', EscapeSequence::FormFeed);
		test_simple_escape_sequence_capital('n', EscapeSequence::Newline);
		test_simple_escape_sequence_capital('r', EscapeSequence::Carriage);
		test_simple_escape_sequence_capital('t', EscapeSequence::Tab);
		test_simple_escape_sequence_capital('v', EscapeSequence::VerticalTab);
		test_simple_escape_sequence_capital('z', EscapeSequence::SkipWhitespace);
		assert_that!("\\\\", parse_escape_sequence).is(Done("", EscapeSequence::BackSlash));
		assert_that!("\\\"", parse_escape_sequence).is(Done("", EscapeSequence::DoubleQuote));
		assert_that!("\\\'", parse_escape_sequence).is(Done("", EscapeSequence::SingleQuote));
		assert_that!("\\\n", parse_escape_sequence).is(Done("", EscapeSequence::LineBreak));
		assert_that!("\\", parse_escape_sequence).is(Done("", EscapeSequence::EmptyEscape));

		assert_that!("\\abc", parse_escape_sequence).is(Done("bc", EscapeSequence::Bell));
		assert_that!("\\\\\\", parse_escape_sequence).is(Done("\\", EscapeSequence::BackSlash));
		assert_that!("\\a b \\c", parse_escape_sequence).is(Done(" b \\c", EscapeSequence::Bell));
		
		assert_that!("\\g", parse_escape_sequence).is(Done("", EscapeSequence::InvalidEscape('g')));
		assert_that!("\\;", parse_escape_sequence).is(Done("", EscapeSequence::InvalidEscape(';')));
		assert_that!("\\++", parse_escape_sequence).is(Done("+", EscapeSequence::InvalidEscape('+')));

		assert_is_true!("", parse_escape_sequence => is_incomplete);
	}

	fn test_simple_escape_sequence_capital(c: char, expected: EscapeSequence) {
		assert_that!(&format!("\\{}", c.to_lowercase()), parse_escape_sequence).is(Done("", expected.clone()));
		assert_that!(&format!("\\{}", c.to_uppercase()), parse_escape_sequence).is(Done("", expected));
	}

	#[test]
	fn test_compound_escape_sequence() {
		assert_that!("\\u{5}", parse_escape_sequence).is(Done("", EscapeSequence::Unicode(5)));
		assert_that!("\\uz", parse_escape_sequence)
			.is(Done("z", EscapeSequence::UnicodeWithoutBrackets("".to_string(), MissingBracket::Both)));

		assert_that!("\\xAff", parse_escape_sequence).is(Done("f", EscapeSequence::Byte(175)));
		assert_that!("\\xza", parse_escape_sequence).is(Done("", EscapeSequence::InvalidHexByte('z', 'a')));

		assert_is_true!("\\xA", parse_escape_sequence => is_incomplete);
		assert_is_true!("\\x", parse_escape_sequence => is_incomplete);


		assert_that!("\\045", parse_escape_sequence).is(Done("", EscapeSequence::Byte(45)));
		assert_that!("\\12345", parse_escape_sequence).is(Done("45", EscapeSequence::Byte(123)));
		assert_that!("\\0abc", parse_escape_sequence)
			.is(Done("c", EscapeSequence::InvalidDecimalByte('0', 'a', 'b')));
		assert_that!("\\999", parse_escape_sequence).is(Done("", EscapeSequence::ByteTooLarge(999)));

		assert_is_true!("\\00", parse_escape_sequence => is_incomplete);
		assert_is_true!("\\0", parse_escape_sequence => is_incomplete);
	}

	#[test]
	fn test_decimal_byte_parse() {
		use self::EscapeSequence::{Byte, ByteTooLarge, InvalidDecimalByte};

		assert_that!("100", parse_decimal_byte_escape).is(Done("", Byte(100)));
		assert_that!("201", parse_decimal_byte_escape).is(Done("", Byte(201)));
		assert_that!("042", parse_decimal_byte_escape).is(Done("", Byte(42)));
		assert_that!("000", parse_decimal_byte_escape).is(Done("", Byte(0)));
		assert_that!("007f", parse_decimal_byte_escape).is(Done("f", Byte(7)));
		assert_that!("1111", parse_decimal_byte_escape).is(Done("1", Byte(111)));

		assert_that!("455", parse_decimal_byte_escape).is(Done("", ByteTooLarge(455)));
		assert_that!("999", parse_decimal_byte_escape).is(Done("", ByteTooLarge(999)));
		assert_that!("260b", parse_decimal_byte_escape).is(Done("b", ByteTooLarge(260)));

		assert_that!("5bb", parse_decimal_byte_escape).is(Done("", InvalidDecimalByte('5', 'b', 'b')));
		assert_that!("4 45", parse_decimal_byte_escape).is(Done("5", InvalidDecimalByte('4', ' ', '4')));

		assert_is_true!("1", parse_decimal_byte_escape => is_incomplete);
		assert_is_true!("40", parse_decimal_byte_escape => is_incomplete);
		assert_is_true!("9b", parse_decimal_byte_escape => is_incomplete);
		assert_is_true!("7-", parse_decimal_byte_escape => is_incomplete);
		assert_is_true!("", parse_decimal_byte_escape => is_incomplete);
		
		assert_is_true!("abc", parse_decimal_byte_escape => is_err);
		assert_is_true!("a", parse_decimal_byte_escape => is_err);
		assert_is_true!("-89", parse_decimal_byte_escape => is_err);
		assert_is_true!("z14", parse_decimal_byte_escape => is_err);
	}

	#[test]
	fn test_hex_byte_parse() {
		use self::EscapeSequence::{Byte, InvalidHexByte};

		assert_that!("x00", parse_hex_byte_escape).is(Done("", Byte(0)));
		assert_that!("x45", parse_hex_byte_escape).is(Done("", Byte(69)));
		assert_that!("x0A", parse_hex_byte_escape).is(Done("", Byte(10)));
		assert_that!("xff", parse_hex_byte_escape).is(Done("", Byte(255)));
		assert_that!("XAe", parse_hex_byte_escape).is(Done("", Byte(174)));
		assert_that!("X4b4", parse_hex_byte_escape).is(Done("4", Byte(75)));
		assert_that!("x2222", parse_hex_byte_escape).is(Done("22", Byte(34)));

		assert_that!("xg9", parse_hex_byte_escape).is(Done("", InvalidHexByte('g', '9')));
		assert_that!("X++", parse_hex_byte_escape).is(Done("", InvalidHexByte('+', '+')));
		assert_that!("xK44", parse_hex_byte_escape).is(Done("4", InvalidHexByte('K', '4')));
		assert_that!("x 7", parse_hex_byte_escape).is(Done("", InvalidHexByte(' ', '7')));

		assert_is_true!("x", parse_hex_byte_escape => is_incomplete);
		assert_is_true!("x4", parse_hex_byte_escape => is_incomplete);
		assert_is_true!("xz", parse_hex_byte_escape => is_incomplete);
		assert_is_true!("", parse_hex_byte_escape => is_incomplete);

		assert_is_true!("444", parse_hex_byte_escape => is_err);
		assert_is_true!("ef", parse_hex_byte_escape => is_err);
		assert_is_true!("b", parse_hex_byte_escape => is_err);
	}

	#[test]
	fn test_unicode_escape() {
		use self::MissingBracket::{Start, End, Both};
		use self::EscapeSequence::{Unicode, UnicodeWithoutBrackets, InvalidUnicode, UnicodeTooLarge};

		assert_that!("u{4}", parse_unicode_escape).is(Done("", Unicode(4)));
		assert_that!("u{09}", parse_unicode_escape).is(Done("", Unicode(9)));
		assert_that!("u{Ff}", parse_unicode_escape).is(Done("", Unicode(255)));
		assert_that!("u{555}", parse_unicode_escape).is(Done("", Unicode(1365)));
		assert_that!("U{0}", parse_unicode_escape).is(Done("", Unicode(0)));
		assert_that!("U{4344}fff", parse_unicode_escape).is(Done("fff", Unicode(17220)));
		assert_that!("u{ffff0}} abc", parse_unicode_escape).is(Done("} abc", Unicode(1048560)));
		assert_that!("u{ffffffffffffffff}", parse_unicode_escape).is(Done("", Unicode(u64::max_value())));
		assert_that!("u{000ffffffffffffffff}", parse_unicode_escape).is(Done("", Unicode(u64::max_value())));

		assert_that!("u{1ffffffffffffffff}", parse_unicode_escape).is(Done("", UnicodeTooLarge("1ffffffffffffffff".to_string())));
		assert_that!("u", parse_unicode_escape).is(Done("", UnicodeWithoutBrackets("".to_string(), Both)));
		assert_that!("U}", parse_unicode_escape).is(Done("", UnicodeWithoutBrackets("".to_string(), Start)));
		assert_that!("u{", parse_unicode_escape).is(Done("", UnicodeWithoutBrackets("".to_string(), End)));
		assert_that!("u{}", parse_unicode_escape).is(Done("", InvalidUnicode("".to_string())));
		assert_that!("u44a", parse_unicode_escape).is(Done("", UnicodeWithoutBrackets("44a".to_string(), Both)));
		assert_that!("u{80z", parse_unicode_escape).is(Done("z", UnicodeWithoutBrackets("80".to_string(), End)));
		assert_that!("u{z}", parse_unicode_escape).is(Done("", InvalidUnicode("z".to_string())));

		assert_that!("u{;}", parse_unicode_escape).is(Done(";}", UnicodeWithoutBrackets("".to_string(), End)));

		

		assert_is_true!("a", parse_unicode_escape => is_err);
		assert_is_true!("{}", parse_unicode_escape => is_err);
		assert_is_true!("", parse_unicode_escape => is_err);
	}
}
