
use parse::{parse_string, parse_comment};
use util::{take_c, fail_if};
use failure::Error;
use errors::parse::brackets::*;

// TODO: Test the [ ] and { } capabilities.
named!(pub parse_parens<&str, &str, Error>,
	parse_fail!(
		ErrParseParens,
		do_parse!(
			cast_fail!(tag_s!("(")) >>

			content: recognize!(
				many0!(
					alt!(
						map!(parse_string, |_| ()) |
						map!(parse_comment, |_| ()) |
						map!(parse_parens, |_| ()) |

						// Take one character, then repeat.
						do_parse!(
							c: take_c >>
							call!(fail_if, "()[]{}".contains(c)) >>
							()
						)
					)
				)
			) >>

			cast_fail!(tag_s!(")")) >>
			(content)
		)
	)
);

#[cfg(test)]
mod test {
	
	use nom::IResult::Done;
	
	
	#[test]
	fn test_parse_parens() {
		use super::parse_parens;
		
		assert_that!("(Hello!)", parse_parens).is(Done("", "Hello!"));
		assert_that!("(Nested strings: \"Hello!\")", parse_parens).is(Done("", "Nested strings: \"Hello!\""));
		assert_that!("(newline\n stuff) extra stuff", parse_parens).is(Done(" extra stuff", "newline\n stuff"));
		assert_that!("(literals [=[too)]=])", parse_parens).is(Done("", "literals [=[too)]=]"));
		assert_that!("()", parse_parens).is(Done("", ""));
		assert_that!("())", parse_parens).is(Done(")", ""));
		assert_that!("(Comments: --stuff\nother +stuff)", parse_parens)
			.is(Done("", "Comments: --stuff\nother +stuff"));
		

		assert_is_true!("(", parse_parens => is_incomplete);
		assert_is_true!("(Incomplete", parse_parens => is_incomplete);
		assert_is_true!("((nested)", parse_parens => is_incomplete);
		assert_is_true!("(\" \"", parse_parens => is_incomplete);
		assert_is_true!("", parse_parens => is_incomplete);

		assert_is_true!(")", parse_parens => is_err);
		assert_is_true!("Stuff(a)", parse_parens => is_err);
		assert_is_true!("\"Stuff\"", parse_parens => is_err);
	}

}

