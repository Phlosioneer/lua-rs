
use parse::{parse_expression, parse_symbol, parse_keyword};
use ast::nodes::{Binop, BinopExpression, Unop, Expression};
use ast::keywords::{Symbol, Keyword};
use util::{many0_whitespace, recognize_until};
use failure::Error;
use errors::parse::operators::*;

named!(pub parse_binop<&str, BinopExpression, Error>,
	parse_fail!(
		ErrParseBinop,
		do_parse!(
			// Find the first binop that isn't inside parentheses.
			first_expr_string: call!(recognize_until, parse_binop_token) >>
			op: parse_binop_token >>
		
			// Parse the stuff we skipped over.
			first_expr: expr_res!(complete!(first_expr_string, do_parse!(
					expr: parse_expression >>
					many0_whitespace >>
					(expr)
				)).to_full_result()) >>

			many0_whitespace >>
			second_expr: parse_expression >>
			(BinopExpression::new(first_expr, op, second_expr))
		)
	)
);

named!(pub parse_unop<&str, (Unop, Expression), Error>,
	parse_fail!(
		ErrParseUnop,
		do_parse!(
			op: parse_unop_token >>
			many0_whitespace >>
			expression: parse_expression >>
			(op, expression)
		)
	)
);


named!(parse_binop_token<&str, Binop, Error>,
	parse_fail!(
		ErrParseBinopToken,
		alt!(
			do_parse!(
				symbol: parse_symbol >>

				// TODO: Propogate the Symbol in the IResult,
				// so that it can be used in an error message.
				op: cast_fail!(expr_opt!(match symbol {
					Symbol::Plus => Some(Binop::Add),
					Symbol::Minus => Some(Binop::Sub),
					Symbol::Star => Some(Binop::Mult),
					Symbol::ForwardSlash => Some(Binop::Div),
					Symbol::DoubleForward => Some(Binop::FloorDiv),
					Symbol::Carot => Some(Binop::Power),
					Symbol::Percent => Some(Binop::Mod),
					Symbol::Amp => Some(Binop::BitAnd),
					Symbol::Bar => Some(Binop::BitOr),
					Symbol::Tilde => Some(Binop::BitXOr),
					Symbol::DoubleGreater => Some(Binop::ShiftR),
					Symbol::DoubleLess => Some(Binop::ShiftL),
					Symbol::DotDot => Some(Binop::Concat),
					Symbol::Less => Some(Binop::LessThan),
					Symbol::LessEqual => Some(Binop::LessThanEqual),
					Symbol::Greater => Some(Binop::GreaterThan),
					Symbol::GreaterEqual => Some(Binop::GreaterThanEqual),
					Symbol::DoubleEqual => Some(Binop::Equal),
					Symbol::NotEqual => Some(Binop::NotEqual),
					_ => None
				})) >>
				(op)
			) |

			do_parse!(
				keyword: complete!(parse_keyword) >>
				op: cast_fail!(expr_opt!(match keyword {
					Keyword::And => Some(Binop::LogicAnd),
					Keyword::Or => Some(Binop::LogicOr),
					_ => None
				})) >>
				(op)
			)
		)
	)
);

named!(parse_unop_token<&str, Unop, Error>,
	parse_fail!(
		ErrParseUnopToken,
		alt!(
			do_parse!(
				symbol: parse_symbol >>

				// TODO: Propogate the Symbol in the IResult,
				// so that it can be used in an error message.
				op: cast_fail!(expr_opt!(match symbol {
					Symbol::Minus => Some(Unop::Negative),
					Symbol::Tilde => Some(Unop::BitNot),
					Symbol::HashTag => Some(Unop::LengthOp),
					_ => None
				})) >>
				(op)
			) |
			do_parse!(
				keyword: complete!(parse_keyword) >>
				op: cast_fail!(expr_opt!(match keyword {
					Keyword::Not => Some(Unop::LogicNot),
					_ => None
				})) >>
				(op)
			)
		)
	)
);

#[cfg(test)]
mod test {
	
	
	use nom::IResult::Done;

	#[test]
	fn test_parse_unop_token() {
		use super::parse_unop_token;
		use super::Unop;

		assert_that!("-", parse_unop_token).is(Done("", Unop::Negative));
		assert_that!("~", parse_unop_token).is(Done("", Unop::BitNot));
		assert_that!("#", parse_unop_token).is(Done("", Unop::LengthOp));
		assert_that!("not", parse_unop_token).is(Done("", Unop::LogicNot));
		assert_that!("-45", parse_unop_token).is(Done("45", Unop::Negative));
		assert_that!("not true", parse_unop_token).is(Done(" true", Unop::LogicNot));
		assert_that!("~0xff", parse_unop_token).is(Done("0xff", Unop::BitNot));
		assert_that!("#stuff", parse_unop_token).is(Done("stuff", Unop::LengthOp));
		assert_that!("##", parse_unop_token).is(Done("#", Unop::LengthOp));

		assert_is_true!("", parse_unop_token => is_err);
		assert_is_true!("~=", parse_unop_token => is_err);
		assert_is_true!("notstuff", parse_unop_token => is_err);
		assert_is_true!("random whatever", parse_unop_token => is_err);
		assert_is_true!("-- this is a comment", parse_unop_token => is_err);
		assert_is_true!(" ~fjd", parse_unop_token => is_err);
		assert_is_true!("true", parse_unop_token => is_err);
		assert_is_true!("tru", parse_unop_token => is_err);
		assert_is_true!("no", parse_unop_token => is_err);
	}

	#[test]
	fn test_parse_binop_token() {
		use super::parse_binop_token;
		use super::Binop;

		assert_that!("+", parse_binop_token).is(Done("", Binop::Add));
		assert_that!("-", parse_binop_token).is(Done("", Binop::Sub));
		assert_that!("*", parse_binop_token).is(Done("", Binop::Mult));
		assert_that!("/", parse_binop_token).is(Done("", Binop::Div));
		assert_that!("//", parse_binop_token).is(Done("", Binop::FloorDiv));
		assert_that!("^", parse_binop_token).is(Done("", Binop::Power));
		assert_that!("%", parse_binop_token).is(Done("", Binop::Mod));
		assert_that!("&", parse_binop_token).is(Done("", Binop::BitAnd));
		assert_that!("|", parse_binop_token).is(Done("", Binop::BitOr));
		assert_that!("~", parse_binop_token).is(Done("", Binop::BitXOr));
		assert_that!(">>", parse_binop_token).is(Done("", Binop::ShiftR));
		assert_that!("<<", parse_binop_token).is(Done("", Binop::ShiftL));
		assert_that!("..", parse_binop_token).is(Done("", Binop::Concat));
		assert_that!("<", parse_binop_token).is(Done("", Binop::LessThan));
		assert_that!("<=", parse_binop_token).is(Done("", Binop::LessThanEqual));
		assert_that!(">", parse_binop_token).is(Done("", Binop::GreaterThan));
		assert_that!(">=", parse_binop_token).is(Done("", Binop::GreaterThanEqual));
		assert_that!("==", parse_binop_token).is(Done("", Binop::Equal));
		assert_that!("~=", parse_binop_token).is(Done("", Binop::NotEqual));
		assert_that!("and", parse_binop_token).is(Done("", Binop::LogicAnd));
		assert_that!("or", parse_binop_token).is(Done("", Binop::LogicOr));

		assert_that!("> var", parse_binop_token).is(Done(" var", Binop::GreaterThan));
		assert_that!("<=-54", parse_binop_token).is(Done("-54", Binop::LessThanEqual));
		assert_that!("or true", parse_binop_token).is(Done(" true", Binop::LogicOr));

		assert_is_true!("", parse_binop_token => is_err);
		assert_is_true!("++", parse_binop_token => is_err);
		assert_is_true!("o ", parse_binop_token => is_err);
		assert_is_true!("nonsense", parse_binop_token => is_err);
		assert_is_true!("!", parse_binop_token => is_err);
		assert_is_true!(" and stuff", parse_binop_token => is_err);
		assert_is_true!("5 + 3", parse_binop_token => is_err);
		assert_is_true!("true", parse_binop_token => is_err);
		assert_is_true!("tru", parse_binop_token => is_err);
		assert_is_true!("an", parse_binop_token => is_err);
	}

	#[test]
	fn test_parse_binop() {
		use super::parse_binop;
		use super::{Expression, Binop, BinopExpression};
		use ast::nodes::Number;
		use parse::parse_string;

		assert_that!("4 + 2", parse_binop).is(Done("", BinopExpression::new(
			Expression::Number(Number::Int(4)),
			Binop::Add,
			Expression::Number(Number::Int(2))
		)));
		assert_that!("77.0 - 2", parse_binop).is(Done("", BinopExpression::new(
			Expression::Number(Number::Float(77.0)),
			Binop::Sub,
			Expression::Number(Number::Int(2))
		)));
		assert_that!("\"hi\"^45", parse_binop).is(Done("", BinopExpression::new(
			Expression::StringLiteral(parse_string("\"hi\"").unwrap().1),
			Binop::Power,
			Expression::Number(Number::Int(45))
		)));

		assert_that!("4 <= [[test]] is false", parse_binop).is(Done(" is false", BinopExpression::new(
			Expression::Number(Number::Int(4)),
			Binop::LessThanEqual,
			Expression::StringLiteral(parse_string("[[test]]").unwrap().1)
		)));
		assert_that!("3 * 2 + 80", parse_binop).is(Done("", BinopExpression::new(
			Expression::Number(Number::Int(3)),
			Binop::Mult,
			Expression::Binop(BinopExpression::new(
				Expression::Number(Number::Int(2)),
				Binop::Add,
				Expression::Number(Number::Int(80))
			))
		)));

		assert_is_true!("-4", parse_binop => is_err);
		assert_is_true!("+ + 88.0", parse_binop => is_err);
		assert_is_true!("true", parse_binop => is_err);
	}

	#[test]
	fn test_parse_unop() {
		use super::parse_unop;
		use super::{Expression, Unop};
		use ast::nodes::Number;
		use parse::parse_string;

		assert_that!("-4", parse_unop).is(Done("", (
			Unop::Negative, Expression::Number(Number::Int(4))
		)));
		assert_that!("~\"hi\"", parse_unop).is(Done("", (
			Unop::BitNot, Expression::StringLiteral(parse_string("\"hi\"").unwrap().1)
		)));
		assert_that!("- -6", parse_unop).is(Done("", (
			Unop::Negative, Expression::Unop(Unop::Negative, Box::new(
				Expression::Number(Number::Int(6))
			))
		)));
		assert_that!("not true", parse_unop).is(Done("", (
			Unop::LogicNot, Expression::Bool(true)
		)));

		assert_is_true!("--1", parse_unop => is_err);
		assert_is_true!("true", parse_unop => is_err);
	}
}