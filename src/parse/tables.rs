
use ast::keywords::Symbol;
use ast::nodes::{Table, Field, Expression};
use parse::{parse_exact_symbol, parse_padded_expression};
use parse::{parse_exact_symbols, parse_name};
use util::{many0_whitespace, recognize_until};
use failure::Error;
use errors::parse::tables::*;

named!(pub parse_table<&str, Table, Error>,
	parse_fail!(
		ErrParseTable,
		map!(
			do_parse!(
				call!(parse_exact_symbol, Symbol::OpenCurl) >>
				many0_whitespace >>
				values: cast_fail!(many0!(parse_field_and_comma)) >>
				trailing_value: fixed_opt!(parse_field) >>
				fixed_opt!(parse_field_separator) >>
				call!(parse_exact_symbol, Symbol::CloseCurl) >>
				(values, trailing_value)
			),
			|(mut values, trailing_value)| {
				if let Some(value) = trailing_value {
					values.push(value);
				}
				values
			}
		)
	)
);

named!(parse_field_separator<&str, (), Error>,
	parse_fail!(
		ErrParseFieldSeparator,
		do_parse!(
			many0_whitespace >>
			call!(parse_exact_symbols, &vec![Symbol::Comma, Symbol::SemiColon]) >>
			many0_whitespace >>
			(())
		)
	)
);

named!(parse_field_and_comma<&str, Field, Error>,
	parse_fail!(
		ErrParseFieldAndComma,
		do_parse!(
			field: parse_field >>
			parse_field_separator >>
			(field)
		)
	)
);

named!(pub parse_field<&str, Field, Error>,
	parse_fail!(
		ErrParseField,
		alt!(
			parse_bracket_field |
			parse_named_field |
			parse_array_field
		)
	)
);

named!(parse_bracket_field<&str, Field, Error>,
	parse_fail!(
		ErrParseBracketField,
		do_parse!(
			call!(parse_exact_symbol, Symbol::OpenSquare) >>
			key_str: call!(recognize_until, |i| call!(i, parse_exact_symbol, Symbol::CloseSquare)) >>
			call!(parse_exact_symbol, Symbol::CloseSquare) >>
			key: expr_res!(complete!(key_str, parse_padded_expression).to_full_result()) >>
			many0_whitespace >>
			call!(parse_exact_symbol, Symbol::Equal) >>
			value: parse_field_value >>
			(Field::new_bracketed(key, value))
		)
	)
);

named!(parse_named_field<&str, Field, Error>,
	parse_fail!(
		ErrParseNamedField,
		do_parse!(
			name: parse_name >>
			many0_whitespace >>
			call!(parse_exact_symbol, Symbol::Equal) >>
			value: parse_field_value >>
			(Field::new_named(name, value))
		)
	)
);

named!(parse_array_field<&str, Field, Error>,
	parse_fail!(
		ErrParseArrayField,
		map!(
			parse_field_value,
			Field::new_implicit
		)
	)
);

named!(parse_field_value<&str, Expression, Error>,
	parse_fail!(
		ErrParseFieldValue,
		do_parse!(
			value_str: call!(recognize_until, |i| alt!(i,
				parse_field_separator |
				map!(call!(parse_exact_symbol, Symbol::CloseCurl), |_| ())
			)) >>
			many0_whitespace >>
			value: expr_res!(complete!(value_str, parse_padded_expression).to_full_result()) >>
			(value)
		)
	)
);

#[allow(unused)]
mod test {
	use ast::nodes::{Table, Field, Expression, Number};
	use nom::IResult::Done;
	use parse::parse_expression;
	use util::unwrap_output;

	#[test]
	fn test_parse_array_field() {
		use super::parse_array_field;
		use parse::parse_expression;

		assert_that!("4,", parse_array_field).is(Done(",", 
			Field::new_implicit(Expression::Number(Number::Int(4)))
		));
		assert_that!(" 5 + 2 ,", parse_array_field).is(Done(",",
			Field::new_implicit(unwrap_output(parse_expression("5 + 2")))
		));
		assert_that!("\"\";", parse_array_field).is(Done(";",
			Field::new_implicit(unwrap_output(parse_expression("\"\"")))
		));
		assert_that!("104 } ", parse_array_field).is(Done("} ",
			Field::new_implicit(unwrap_output(parse_expression("104")))
		));
		assert_that!("var,", parse_array_field).is(Done(",",
			Field::new_implicit(unwrap_output(parse_expression("var")))
		));

		assert_is_true!("4", parse_array_field => is_err);
	}

	#[test]
	fn test_parse_named_field() {
		use super::parse_named_field;
		use parse::parse_expression;

		assert_that!("name = 57;", parse_named_field).is(Done(";",
			Field::new_named("name", unwrap_output(parse_expression("57")))
		));
		assert_that!("var1_49=hello,", parse_named_field).is(Done(",",
			Field::new_named("var1_49", unwrap_output(parse_expression("hello")))
		));
		assert_that!("stuff = 4 + 8 * 9 } ", parse_named_field).is(Done("} ",
			Field::new_named("stuff", unwrap_output(parse_expression("4 + 8 * 9")))
		));

		assert_is_true!("fd", parse_named_field => is_err);
		assert_is_true!("whatever = ", parse_named_field => is_err);
		assert_is_true!("x = 4", parse_named_field => is_err);

		assert_is_true!("1illegal_name = 4;", parse_named_field => is_err);
		assert_is_true!("= 4,", parse_named_field => is_err);
		assert_is_true!("2;", parse_named_field => is_err);
		assert_is_true!("whatever = ;", parse_named_field => is_err);

	}

	#[test]
	fn test_parse_bracket_field() {
		use super::parse_bracket_field;
		use parse::parse_expression;

		assert_that!("[5] = 4,", parse_bracket_field).is(Done(",",
			Field::new_bracketed(
				unwrap_output(parse_expression("5")),
				unwrap_output(parse_expression("4"))
			)
		));
		assert_that!("[test]=whatever + 5 ;", parse_bracket_field).is(Done(";",
			Field::new_bracketed(
				unwrap_output(parse_expression("test")),
				unwrap_output(parse_expression("whatever + 5"))
			)
		));

		assert_is_true!(";", parse_bracket_field => is_err);
		assert_is_true!("5", parse_bracket_field => is_err);
		assert_is_true!("[1] ; ", parse_bracket_field => is_err);
		assert_is_true!("[] = stuff", parse_bracket_field => is_err);
	}

	#[test]
	fn test_parse_table() {
		use super::parse_table;
		use parse::parse_expression;
		use ast::nodes::{Field};

		assert_that!("{}", parse_table).is(Done("", Vec::new()));

		// Example from the lua manual, without functions.
		assert_that!("{ [f = 1] = g; \"x\", \"y\"; x = 1, f + x, [30] = 23; 45 }", parse_table).is(Done("",
			vec![
				Field::new_bracketed(
					unwrap_output(parse_expression("f = 1")),
					unwrap_output(parse_expression("g"))
				),
				Field::new_implicit(
					unwrap_output(parse_expression("\"x\""))
				),
				Field::new_implicit(
					unwrap_output(parse_expression("\"y\""))
				),
				Field::new_named(
					"x",
					unwrap_output(parse_expression("1"))
				),
				Field::new_implicit(
					unwrap_output(parse_expression("f + x"))
				),
				Field::new_bracketed(
					unwrap_output(parse_expression("30")),
					unwrap_output(parse_expression("23"))
				),
				Field::new_implicit(
					unwrap_output(parse_expression("45"))
				)
			]
		));
		
	}
}

