
use ast::nodes::{Variable, Expression};
use ast::keywords::Symbol;
use parse::{parse_padded_expression, parse_expression, parse_keyword, parse_exact_symbol};
use util::{fail_if, continue_if, take_c, recognize_until, many0_whitespace};
use failure::Error;
use errors::parse::variables::*;

// Names are any string of alphanumerics and underscores that don't
// start with a number. Note that this does not check if the name is
// already a reserved keyword; that check can be postponed until after
// parsing.
named!(pub parse_name<&str, &str, Error>, 
	parse_fail!(
		ErrParseName,
		recognize!(
			do_parse!(
				first: take_c >>
				call!(continue_if, first.is_alphabetic() || first == '_') >>

				cast_fail!(take_while!(|c: char| c.is_alphanumeric() || c == '_')) >>
				()
			)
		)
	)
);

// This is parse_name, but it will fail if the name is a keyword.
named!(pub parse_name_no_keyword<&str, &str, Error>,
	parse_fail!(
		ErrParseNameNoKeyword,
		do_parse!(
			name: parse_name >>
			call!(fail_if, complete!(name, parse_keyword).is_done()) >>
			(name)
		)
	)
);

named!(parse_member_variable<&str, (Expression, String), Error>,
	parse_fail!(
		ErrParseMemberVariable,
		do_parse!(
			// Note: "." is not technically a symbol in Lua. It's purely for use in member variables.
			object_expr_string: call!(recognize_until, |i| parse_exact_symbol(i, Symbol::Period)) >>
			call!(parse_exact_symbol, Symbol::Period) >>
			many0_whitespace >>
			member_name: parse_name_no_keyword >>
			object_expr: expr_res!(complete!(object_expr_string, parse_padded_expression).to_full_result()) >>
			((object_expr, member_name.to_string(), ))
		)
	)
);

named!(parse_map_variable<&str, (Expression, Expression), Error>,
	parse_fail!(
		ErrParseMapVariable,
		do_parse!(
			map_expr_string: call!(recognize_until, |i| parse_exact_symbol(i, Symbol::OpenSquare)) >>
			call!(parse_exact_symbol, Symbol::OpenSquare) >>
			key_expr: parse_padded_expression >>
			call!(parse_exact_symbol, Symbol::CloseSquare) >>
			map_expr: expr_res!(do_parse!(
				map_expr_string,
				ret: parse_expression >>
				many0_whitespace >>
				(ret)
			).to_full_result()) >>
			((map_expr, key_expr, ))
		)
	)
);

named!(pub parse_variable<&str, Variable, Error>,
	parse_fail!(
		ErrParseVariable,
		alt_complete!(
			map!(
				parse_map_variable,
				|(map_expr, key_expr)| Variable::Map(Box::new(map_expr), Box::new(key_expr))
			) |
			map!(
				parse_member_variable, 
				|(object_expr, member_name)| Variable::Member(Box::new(object_expr), member_name)
			) |
			map!(
				parse_name_no_keyword, 
				|name| Variable::Simple(name.to_string())
			)
		)
	)
);

#[cfg(test)]
mod test {
	use nom::IResult::Done;
	

	#[test]
	fn test_parse_name() {
		use super::parse_name;

		assert_that!("Hello", parse_name).is(Done("", "Hello"));
		assert_that!("whatever", parse_name).is(Done("", "whatever"));
		assert_that!("foo bar baz", parse_name).is(Done(" bar baz", "foo"));
		assert_that!("number42_awesome_VERSION = 0;", parse_name)
			.is(Done(" = 0;", "number42_awesome_VERSION"));
		assert_that!("i j k", parse_name).is(Done(" j k", "i"));
		assert_that!("spaces! ", parse_name).is(Done("! ", "spaces"));
		assert_that!("for i in x", parse_name).is(Done(" i in x", "for"));
		assert_that!("_underscoresAreFun", parse_name).is(Done("", "_underscoresAreFun"));
		assert_that!("_ _", parse_name).is(Done(" _", "_"));

		assert_is_true!("", parse_name => is_incomplete);

		assert_is_true!("22catch", parse_name => is_err);
		assert_is_true!(" ", parse_name => is_err);
		assert_is_true!("4", parse_name => is_err);
		assert_is_true!("    stuff", parse_name => is_err);
		assert_is_true!("++i", parse_name => is_err);
	}
}