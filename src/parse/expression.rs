
use ast::nodes::{Expression, PrefixExpression};
use ast::keywords::{Keyword, Symbol};
use parse::{parse_keyword, parse_symbol, parse_string, parse_number, parse_variable};
use parse::{parse_name, parse_binop, parse_unop, parse_parens};
use util::{continue_if, many0_whitespace};
use failure::Error;
use errors::parse::expression::*;

named!(pub parse_prefix_expression<&str, PrefixExpression, Error>,
	parse_fail!(
		ErrParsePrefixExpression,
		alt_complete!(
			// Function Call
			// Note: This is parsed first because it must be "greedy".
			// See the note on the FuncCall structure.
			//map!(parse_func_call, |f| Expression::FuncCall(f)) |
	
			// Variable
			map!(parse_variable, |v| PrefixExpression::Variable(v)) |

			// Sub-expression, surrounded by parentheses
			do_parse!(
				subexpression: complete!(parse_parens) >>
				expression: expr_res!(complete!(subexpression, parse_padded_expression).to_full_result()) >>
				(PrefixExpression::SubExpression(Box::new(expression)))
			)
		)
	)
);

// Convenience function to automatically trim whitespace around an expression. Note: It matches
// 0 or more spaces on both sides, so it only makes sense to use this when parsing an expression
// surrounded by delimeters of some kind.
named!(pub parse_padded_expression<&str, Expression, Error>,
	parse_fail!(
		ErrParsePaddedExpression,
		complete!(
			do_parse!(
				many0_whitespace >>
				expression: parse_expression >>
				many0_whitespace >>
				(expression)
			)
		)
	)
);

// TODO: Does the ordering matter?
// Note: Parsing this does not remove the whitespace on either side of the expression.
// If the input string starts with whitespace, this parser will fail.
named!(pub parse_expression<&str, Expression, Error>,
	parse_fail!(
		ErrParseExpression,
		alt_complete!(
			// Binary Operation. This is the most "greedy" expression parser, because
			// otherwise expressions like "(4) + 3" would parse the prefix expression
			// (4) and then stop.
			//
			// There are no situations where parse_binop will fail to parse due to a
			// prefix expression, because parse_binop uses a bracket-aware search
			// for the first binop.
			map!(parse_binop, |op_expression| Expression::Binop(op_expression)) |

			// Prefix expressions are "greedy"; see comment in parse_prefix_expression.
			// If a sub-expression is found, flatten it.
			map!(parse_prefix_expression,
				|prefix| match prefix {
					PrefixExpression::FuncCall(func_call_struct) => Expression::FuncCall(func_call_struct),
					PrefixExpression::Variable(variable_struct) => Expression::Variable(variable_struct),
					PrefixExpression::SubExpression(sub_expression) => *sub_expression
				}
			) |

			// Unary Operation
			map!(parse_unop, |(op, expr)| Expression::Unop(op, Box::new(expr))) |

			// Nil
			do_parse!(
				keyword: parse_keyword >>
				call!(continue_if, keyword == Keyword::Nil) >>
				(Expression::Nil)
			) |

			// Boolean literal
			do_parse!(
				keyword: parse_keyword >>
				call!(continue_if, keyword == Keyword::True || keyword == Keyword::False) >>
				(Expression::Bool(keyword == Keyword::True))
			) |

			// Number literal
			do_parse!(
				number: parse_number >>
			
				// Numbers cannot be immediately followed by variables.
				// To check this, take the first character after the number
				// and check if it's the start of a legal Name.
				next_char: cast_fail!(alt!(
					map!(eof!(), |_| " ") |
					peek!(take_s!(1))
				)) >>
				call!(continue_if, parse_name(next_char).is_err()) >>
			
				(Expression::Number(number))
			) |

			// String literal
			map!(parse_string, |s| Expression::StringLiteral(s)) |

			// Ellipsis
			do_parse!(
				symbol: parse_symbol >>
				call!(continue_if, symbol == Symbol::Ellipsis) >>
				(Expression::Ellipsis)
			) //|


			// Table
			//map!(parse_table, |fields| Expression::Table(fields)) |

		
			// Inline Function Definition
			//map!(parse_func_body, |b| Expression::FuncDef(b))
		)
	)
);

#[cfg(test)]
mod test {
	
	
	use nom::IResult::Done;
	use super::Expression;
	use super::parse_expression;

	#[test]
	fn test_failing_expressions() {
		assert_is_true!("", parse_expression => is_err);
		assert_is_true!("  ", parse_expression => is_err);
		assert_is_true!(" 4", parse_expression => is_err);

		assert_is_true!(".9", parse_expression => is_err);

		assert_is_true!("(", parse_expression => is_err);
		assert_is_true!("( )", parse_expression => is_err);
	}

	#[test]
	fn test_parse_trivial_expression() {

		assert_that!("nil", parse_expression).is(Done("", Expression::Nil));
		assert_that!("true", parse_expression).is(Done("", Expression::Bool(true)));
		assert_that!("false", parse_expression).is(Done("", Expression::Bool(false)));
		assert_that!("...", parse_expression).is(Done("", Expression::Ellipsis));

		assert_that!("nil stuff", parse_expression).is(Done(" stuff", Expression::Nil));
		assert_that!("true false", parse_expression).is(Done(" false", Expression::Bool(true)));
		assert_that!("true;1", parse_expression).is(Done(";1", Expression::Bool(true)));
		assert_that!("...54", parse_expression).is(Done("54", Expression::Ellipsis));
	}

	#[test]
	fn test_parse_literal_values() {
		use ast::nodes::{Number, StringLiteral};
		use itertools::Either;

		assert_that!("42", parse_expression).is(Done("", Expression::Number(Number::Int(42))));
		assert_that!("2;", parse_expression).is(Done(";", Expression::Number(Number::Int(2))));
		assert_that!("0.9", parse_expression).is(Done("", Expression::Number(Number::Float(0.9))));
		assert_that!("\"Hello!\"", parse_expression).is(Done("", Expression::StringLiteral(
			StringLiteral::new(vec![Either::Left("Hello!".to_string())])
		)));
	}

	#[test]
	fn test_parse_subexpression() {
		use ast::nodes::{Number, Unop, StringLiteral};
		use itertools::Either;

		assert_that!("(43)", parse_expression).is(Done("", 
			Expression::Number(Number::Int(43))
		));
		assert_that!("( -8) hello", parse_expression).is(Done(" hello", 
			Expression::Unop(
				Unop::Negative,
				Box::new(Expression::Number(Number::Int(8)))
			)
		));
		assert_that!("((5))", parse_expression).is(Done("", 
			Expression::Number(Number::Int(5))
		));
		assert_that!("( \"Hello ) tricky\" )", parse_expression).is(Done("", 
			Expression::StringLiteral(StringLiteral::new(
				vec![Either::Left("Hello ) tricky".to_string())]
			))
		));

	}

	#[test]
	fn test_parse_operators() {
		use ast::nodes::{Number, Unop, Binop, BinopExpression};

		assert_that!("5 * 1.3", parse_expression).is(Done("",
			Expression::Binop(BinopExpression::new(
				Expression::Number(Number::Int(5)),
				Binop::Mult,
				Expression::Number(Number::Float(1.3))
			))
		));
		assert_that!("4 > -3", parse_expression).is(Done("",
			Expression::Binop(BinopExpression::new(
				Expression::Number(Number::Int(4)),
				Binop::GreaterThan,
				Expression::Unop(
					Unop::Negative,
					Box::new(Expression::Number(Number::Int(3)))
				)
			))
		));
		assert_that!("(true and false) == true", parse_expression).is(Done("",
			Expression::Binop(BinopExpression::new(
				Expression::Binop(BinopExpression::new(
					Expression::Bool(true),
					Binop::LogicAnd,
					Expression::Bool(false)
				)),
				Binop::Equal,
				Expression::Bool(true)
			))
		));
		assert_that!("true == (true and false)", parse_expression).is(Done("",
			Expression::Binop(BinopExpression::new(
				Expression::Bool(true),
				Binop::Equal,
				Expression::Binop(BinopExpression::new(
					Expression::Bool(true),
					Binop::LogicAnd,
					Expression::Bool(false)
				))
			))
		));
		assert_that!("5 + 3 + 1", parse_expression).is(Done("",
			Expression::Binop(BinopExpression::new(
				Expression::Number(Number::Int(5)),
				Binop::Add,
				Expression::Binop(BinopExpression::new(
					Expression::Number(Number::Int(3)),
					Binop::Add,
					Expression::Number(Number::Int(1))
				))
			))
		));

		// TODO: Add some examples with unparsed stuff? (i.e. Done("blah", _))
	}

	#[test]
	fn test_parse_variable() {
		use ast::nodes::{Number, Binop, BinopExpression, Variable};

		assert_that!("foo + bar - 3", parse_expression).is(Done("",
			Expression::Binop(BinopExpression::new(
				Expression::Variable(Variable::Simple("foo".to_string())),
				Binop::Add,
				Expression::Binop(BinopExpression::new(
					Expression::Variable(Variable::Simple("bar".to_string())),
					Binop::Sub,
					Expression::Number(Number::Int(3))
				))
			))
		));
		assert_that!("foo.bar == 5", parse_expression).is(Done("",
			Expression::Binop(BinopExpression::new(
				Expression::Variable(Variable::Member(
					Box::new(Expression::Variable(Variable::Simple("foo".to_string()))),
					"bar".to_string()
				)),
				Binop::Equal,
				Expression::Number(Number::Int(5))
			))
		));
		assert_that!("foo[5] + bar", parse_expression).is(Done("",
			Expression::Binop(BinopExpression::new(
				Expression::Variable(Variable::Map(
					Box::new(Expression::Variable(Variable::Simple("foo".to_string()))),
					Box::new(Expression::Number(Number::Int(5)))
				)),
				Binop::Add,
				Expression::Variable(Variable::Simple("bar".to_string()))
			))
		));
	}
}

