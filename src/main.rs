// TODO: Remove this when we're using the main() function.
#![allow(dead_code)]

#![feature(specialization)]

#![feature(try_from)]

#[macro_use]
extern crate nom;

#[macro_use]
extern crate failure;

extern crate itertools;

#[cfg(test)]
#[macro_use] mod assert;

#[macro_use] mod errors;
#[macro_use] mod util;
mod ast;
mod parse;



fn main() {
    println!("Hello, world!");
	
}

