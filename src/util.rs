
use std::vec::Vec;
use nom::{IResult, ErrorKind};
use parse::parse_parens;	// TODO: Get rid of this dependency?
use failure::Error;
use errors::util::*;
use errors::fn_parse_fail;

#[allow(unused)]
macro_rules! expr {
	($i:expr, $e:expr) => (call!($i, $crate :: util :: expr_impl, $e))
}

// Takes an array of strings and concatenates them.
pub fn collect_string_vec(array: &Vec<&str>) -> String {

	// A rough approximation.
	let mut ret = String::with_capacity(array.len());

	array.iter().for_each(|this| ret.push_str(this));
	ret
}

// Return an error and stop parsing if the condition is not met. Meant for use
// inside do_parse.
pub fn continue_if<T: ?Sized>(i: &T, condition: bool) -> IResult<&T, (), Error> {
	if condition {
		IResult::Done(i, ())
	} else {
		IResult::Error(ErrorKind::Custom(ErrContinueIf().into()))
	}
}

// Return an error and stop parsing if the condition is met. Meant for use inside
// do_parse.
pub fn fail_if<T: ?Sized>(i: &T, condition: bool) -> IResult<&T, (), Error> {
	parse_fail!(
		i,
		ErrFailIf,
		call!(continue_if, !condition)
	)
}

// Takes exactly 1 character. May return incomplete.
named!(pub take_c<&str, char, Error>,
	parse_fail!(
		ErrTakeC,
		cast_fail!(map!(
			take_s!(1),
			|s| s.chars().next().unwrap()
		))
	)
);

// Matches 0 or more whitespace characters.
named!(pub many0_whitespace<&str, &str, Error>,
	parse_fail!(
		ErrMany0Whitespace,
		cast_fail!(take_while!(|c: char| c.is_whitespace()))
	)
);

// Matches 1 or more whitespace characters.
named!(pub many1_whitespace<&str, &str, Error>,
	parse_fail!(
		ErrMany1Whitespace,
		recognize!(
			do_parse!(
				first: take_c >>
				call!(continue_if, first.is_whitespace()) >>
				many0_whitespace >>
				()
			)
		)
	)
);

// Matches exactly 1 whitespace character.
named!(pub take_c_ws<&str, char, Error>,
	parse_fail!(
		ErrTakeCWs,
		do_parse!(
			c: take_c >>
			call!(continue_if, c.is_whitespace()) >>
			(c)
		)
	)
);

// This will consume input until the sub-parser F matches, skipping matching
// parentheses. It will not consume the match of the sub-parser. It will never
// return a string of length 0.
//
// Example usage: call!(recognize_until, |input| tag_s!(input, "."))
//
// TODO: Write tests. In particular, ensure that it still works if the sub-parser
// is expecting a parentheses (like tag_s!("(")).
// TODO: Remove dependency on parse_parens. Maybe make this take a function to
// decide when to skip? But it needs to keep track of state...
// TODO: Make a version that consumes the subparser and returns the result, too.
// Note: The lifetime specifier 'a essentially tells the compiler "these are all
// slices of the same string, so don't worry about returning references"
pub fn recognize_until<'a, F, T>(i: &'a str, sub: F) -> IResult<&'a str, &'a str, Error>
	where
		F: Fn(&'a str) -> IResult<&'a str, T, Error>
{

	let ret: IResult<&'a str, &'a str, Error> =	recognize!(
		i,
		many1!(
			do_parse!(
				maybe_op: complete!(
					peek!(
						fixed_opt!(
							sub
						)
					)
				) >>
				call!(fail_if, maybe_op.is_some()) >>

				rest: alt!(
					parse_parens |
					cast_fail!(take_s!(1))
				) >>
				(rest)
			)
		)
	);

	let ret = fn_parse_fail(ret, |e| ErrRecognizeUntil::Chained(e));

	// Confirm that we DID find a sub-parser match.
	if let IResult::Done(ref remaining, _) = ret {
		if remaining.len() == 0 {
			return IResult::Error(ErrorKind::Custom(ErrRecognizeUntil::SubParserNotFound.into()))
		}
	}

	ret
}

// Unwraps an IResult into just the output, discarding the remaining string.
pub fn unwrap_output<A, B, C>(result: IResult<A, B, C>) -> B {
	let (_, ret) = result.unwrap();
	ret
}

// Purely for macro machinery, do not use.
pub fn expr_impl<T, E>(i: &str, expr: T) -> IResult<&str, T, E> {
	IResult::Done(i, expr)
}

#[cfg(test)]
mod test {
	use super::IResult::Done;
	

	#[test]
	fn test_collect_string_vec() {
		use super::collect_string_vec;

		assert_that!(&vec!["Hello", ", ", "world!"], collect_string_vec).is("Hello, world!".to_string());
		assert_that!(&vec!["Just one string."], collect_string_vec).is("Just one string.".to_string());
		assert_that!(&vec!["Empty", "", " string"], collect_string_vec).is("Empty string".to_string());
		assert_that!(&vec![], collect_string_vec).is("".to_string());
	}

	#[test]
	fn test_continue_if() {
		use super::continue_if;

		assert_that!(call!("Hello", continue_if, true)).is(Done("Hello", ()));
		assert_that!(call!("", continue_if, true)).is(Done("", ()));
		assert_is_true!(call!("Hello", continue_if, false) => is_err);
		assert_is_true!(call!("", continue_if, false) => is_err);
	}

	#[test]
	fn test_fail_if() {
		use super::fail_if;

		assert_that!(call!("Hello", fail_if, false)).is(Done("Hello", ()));
		assert_that!(call!("", fail_if, false)).is(Done("", ()));
		assert_is_true!(call!("Hello", fail_if, true) => is_err);
		assert_is_true!(call!("", fail_if, true) => is_err);
	}

	#[test]
	fn test_take_c() {
		use super::take_c;

		assert_that!("Hello", take_c).is(Done("ello", 'H'));
		assert_that!("W", take_c).is(Done("", 'W'));
		assert_that!("\n Hi", take_c).is(Done(" Hi", '\n'));
		assert_is_true!("", take_c => is_incomplete);
	}

	#[test]
	fn test_many0_whitespace() {
		use super::many0_whitespace;

		assert_that!("   ", many0_whitespace).is(Done("", "   "));
		assert_that!("\n  \t blah", many0_whitespace).is(Done("blah", "\n  \t "));
		assert_that!("foo", many0_whitespace).is(Done("foo", ""));
		assert_that!("bar ", many0_whitespace).is(Done("bar ", ""));
		assert_that!("", many0_whitespace).is(Done("", ""));
	}

	#[test]
	fn test_many1_whitespace() {
		use super::many1_whitespace;

		assert_that!("   ", many1_whitespace).is(Done("", "   "));
		assert_that!("\n  \t blah", many1_whitespace).is(Done("blah", "\n  \t "));
		assert_is_true!("foo", many1_whitespace => is_err);
		assert_is_true!("bar ", many1_whitespace => is_err);
		assert_is_true!("", many1_whitespace => is_incomplete);
	}

	#[test]
	fn test_take_c_ws() {
		use super::take_c_ws;

		assert_that!("\t", take_c_ws).is(Done("", '\t'));
		assert_that!("   ", take_c_ws).is(Done("  ", ' '));
		assert_that!("\n  \t blah", take_c_ws).is(Done("  \t blah", '\n'));
		assert_is_true!("foo", take_c_ws => is_err);
		assert_is_true!("bar ", take_c_ws => is_err);
		assert_is_true!("", take_c_ws => is_incomplete);
	}
}