
use failure::{Error, Fail};
use nom::{ErrorKind, IResult};

// Takes in an enum variant name / tuple struct name $err and adds it to the chain.
//
// Technically, $err can be any `FnOnce(cause: failure::Error) -> failure::Fail`.
//
macro_rules! parse_fail (
	// Append $err to the error chain.
	($i:expr, $err:expr, $submac:ident ! ( $($args:tt)* ) ) => (
		{
			let res: ::nom::IResult<_, _, ::failure::Error> = $submac!($i, $($args)*);
			match res {
				::nom::IResult::Incomplete(x) => ::nom::IResult::Incomplete(x),
				::nom::IResult::Done(i, o) => ::nom::IResult::Done(i, o),
				::nom::IResult::Error(err) => {
					let old_error: ::failure::Error = $crate ::errors::flatten_error(err);
					let new_error: ::failure::Error = $err(old_error).into();
					::nom::IResult::Error(::nom::ErrorKind::Custom(new_error))
				}
			}
		}
	);
	// Same forwarding as nom.
	($i:expr, $err:expr, $f:expr) => (
		parse_fail!($i, $err, call!($f));
	);
);

macro_rules! cast_fail (
	// Convert nom errors to Error
	($i:expr, $submac:ident ! ( $($args:tt)* ) ) => (
		{
			let res: ::nom::IResult<_, _, u32> = $submac!($i, $($args)*);
			match res {
				::nom::IResult::Incomplete(x) => ::nom::IResult::Incomplete(x),
				::nom::IResult::Done(i, o) => ::nom::IResult::Done(i, o),
				::nom::IResult::Error(err) => {
					let wrapped_err: ::failure::Error = err.into();
					::nom::IResult::Error(::nom::ErrorKind::Custom(wrapped_err))
				}
			}
		}
	);
	// Same forwarding as nom.
	($i: expr, $f:expr) => (
		parse_fail!($i, call!($f));
	)
);

// Nom::opt! has a bug where it doesn't propogate the error type of IResults.
// This is fixed in nom 4.0
macro_rules! fixed_opt (
	($i:expr,  $($rest:tt)+ ) => (
		cast_fail!($i, opt!($($rest)+));
	);
);

// TODO: I think there might be a similar bug in many1, maybe also many0, cond, cond_reduce?

// This is the function form of parse_fail!(i, Err, <parser>).
#[allow(unused)]
pub fn fn_parse_fail<T, E, F>(original: IResult<&str, T, Error>, error_constructor: F) -> IResult<&str, T, Error>
	where F: FnOnce(Error) -> E,
		  E: Fail
{
	match original {
		IResult::Incomplete(x) => IResult::Incomplete(x),
		IResult::Done(i, o) => IResult::Done(i, o),
		IResult::Error(err) => {
			let old_error: Error = flatten_error(err);
			let new_error: Error = error_constructor(old_error).into();
			IResult::Error(ErrorKind::Custom(new_error))
		}
	}
}

// This is the function form of cast_fail!(i, <parser>).
#[allow(unused)]
pub fn fn_cast_fail<T>(original: IResult<&str, T, u32>) -> IResult<&str, T, Error> {
	match original {
		IResult::Incomplete(x) => IResult::Incomplete(x),
		IResult::Done(i, o) => IResult::Done(i, o),
		IResult::Error(err) => {
			let wrapped_err: Error = err.into();
			IResult::Error(ErrorKind::Custom(wrapped_err))
		}
	}
}

// If the nom error is a wrapper around an Error object, return that object. Otherwise, return
// a new Error object wrapping around this nom error.
pub fn flatten_error(nom_error: ErrorKind<Error>) -> Error {
	if let ErrorKind::Custom(original) = nom_error {
		original
	} else {
		nom_error.into()
	}
}


macro_rules! make_chained_fail (
	($name:ident, $display:expr) => (
		#[derive(Fail, Debug, Display)]
		#[display(fmt = $display)]
		pub struct $name(#[flat_cause] pub ::failure::Error);
	);
);

pub mod util {

	use failure::Error;

	#[derive(Fail, Debug, Display)]
	#[display(fmt = "continue_if")]
	pub struct ErrContinueIf();
	
	make_chained_fail!(ErrFailIf, "fail_if");
	make_chained_fail!(ErrTakeC, "take_c");
	make_chained_fail!(ErrMany0Whitespace, "many0_whitespace");
	make_chained_fail!(ErrMany1Whitespace, "many1_whitespace");
	make_chained_fail!(ErrTakeCWs, "take_c_ws");
	//make_chained_fail!(RecognizeUntil, "recognize_until");

	#[derive(Fail, Debug, Display)]
	pub enum ErrRecognizeUntil {
		#[display(fmt = "regocnize_until: Sub parser not found")]
		SubParserNotFound,
		
		#[display(fmt = "recognize_until")]
		Chained(#[flat_cause] Error)
	}
}

pub mod parse {
	pub mod brackets {

		make_chained_fail!(ErrParseParens, "parse_parens");
	}

	pub mod expression {

		make_chained_fail!(ErrParseExpression, "parse_expression");
		make_chained_fail!(ErrParsePrefixExpression, "parse_prefix_expression");
		make_chained_fail!(ErrParsePaddedExpression, "parse_padded_expression");
	}

	pub mod operators {
	
		make_chained_fail!(ErrParseBinop, "parse_binop");
		make_chained_fail!(ErrParseUnop, "parse_unop");
		make_chained_fail!(ErrParseBinopToken, "parse_binop_token");
		make_chained_fail!(ErrParseUnopToken, "parse_unop_token");
	}

	pub mod tables {
	
		make_chained_fail!(ErrParseTable, "parse_table");
		make_chained_fail!(ErrParseFieldSeparator, "parse_field_separator");
		make_chained_fail!(ErrParseFieldAndComma, "parse_field_and_comma");
		make_chained_fail!(ErrParseField, "parse_field");
		make_chained_fail!(ErrParseBracketField, "parse_bracket_field");
		make_chained_fail!(ErrParseNamedField, "parse_named_field");
		make_chained_fail!(ErrParseArrayField, "parse_array_field");
		make_chained_fail!(ErrParseFieldValue, "parse_field_value");
	}

	pub mod variables {
	
		make_chained_fail!(ErrParseName, "parse_name");
		make_chained_fail!(ErrParseNameNoKeyword, "parse_name_no_keyword");
		make_chained_fail!(ErrParseMemberVariable, "parse_member_variable");
		make_chained_fail!(ErrParseMapVariable, "parse_map_variable");
		make_chained_fail!(ErrParseVariable, "parse_variable");
	}

	pub mod primitives {
	
		pub mod comments {
			
			make_chained_fail!(ErrParseComment, "parse_comment");
		}

		pub mod decimal_numbers {
			
			make_chained_fail!(ErrRecognizeIntNumberDec, "recognize_int_number_dec");
			make_chained_fail!(ErrRecognizeSignedIntNumberDec, "recognize_signed_int_number_dec");
			make_chained_fail!(ErrRecognizeFloatNumberDec, "recognize_float_number_dec");
			make_chained_fail!(ErrRecognizeScienceNumberDec, "recognize_science_number_dec");
			make_chained_fail!(ErrParseIntNumberDec, "parse_int_number_dec");
			make_chained_fail!(ErrParseFloatNumberDec, "parse_float_number_dec");
			make_chained_fail!(ErrParseScienceNumberDec, "parse_science_number_dec");
			make_chained_fail!(ErrParseNumberDec, "parse_number_dec");
		}

		pub mod numbers {
			make_chained_fail!(ErrParseNumber, "parse_number");
		}

		pub mod keywords {
		
			make_chained_fail!(ErrParseKeyword, "parse_keyword");
		}

		pub mod labels {
			
			make_chained_fail!(ErrParseLabel, "parse_label");
			make_chained_fail!(ErrParseGoto, "parse_goto");
		}

		pub mod string {
			
			make_chained_fail!(ErrParseStringContent, "parse_string_content");
			make_chained_fail!(ErrNotEscapeOrQuote, "not_escape_or_quote");
			make_chained_fail!(ErrParseNormalString, "parse_normal_string");
			make_chained_fail!(ErrParseLongBracketString, "parse_long_bracket_string");
			make_chained_fail!(ErrParseString, "parse_string");
		}

		pub mod string_escapes {
			
			make_chained_fail!(ErrParseDecimalByteEscape, "parse_decimal_byte_escape");
			make_chained_fail!(ErrParseHexByteEscape, "parse_hex_byte_escape");
			make_chained_fail!(ErrMatchHex, "match_hex");
			make_chained_fail!(ErrParseUnicodeEscape, "parse_unicode_escape");
			make_chained_fail!(ErrParseEscapeSequence, "parse_escape_sequence");
		}

		pub mod symbols {
			
			make_chained_fail!(ErrParseSimpleSymbol, "parse_simple_symbol");
			make_chained_fail!(ErrParseOpenLongBracket, "parse_open_long_bracket");
			make_chained_fail!(ErrParseCloseLongBracket, "parse_close_long_bracket");
			make_chained_fail!(ErrParseSymbol, "parse_symbol");
			make_chained_fail!(ErrParseExactSymbols, "parse_exact_symbol");
		}
	}
}



