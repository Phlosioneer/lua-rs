﻿
use std::vec::Vec;
use std::fmt::Debug;
use std::cmp::PartialEq;
use std::cmp;

// An assertion system based on the spectral crate.
// TODO: Test this module.

#[must_use]
pub struct Assertion<T> {
	pub input: Option<String>,
	pub subject: T,
	pub file: Option<String>,
	pub line: Option<String>,
	pub code: Option<String>
}

#[must_use]
pub struct MappedAssertion<T, U> {
	original: Assertion<T>,
	mapped_value: U,
	code: Option<String>,
	chain: Vec<(String, Option<String>)>
}

struct AssertionFailure {
	input: Option<String>,
	input_code: Option<String>,
	operation: String,
	expected: String,
	actual: String,
	file: Option<String>,
	line: Option<String>,
	chain: Vec<(String, Option<String>)>
}

#[allow(unused_macros)]
macro_rules! new_assert_that {
	// Basic usage.
	($test_func:ident ( $($input:tt)* ) $($rest:tt)* ) => (
		{
			let line = line!().to_string();
			let file = file!().to_string();
			let assertion = $crate :: assert::Assertion::new_from_input(
				$($input)*, 
				|i| $test_func(&i)
			).set_location(file, line).set_input_code(stringify!($($input)*));
			new_assert_that!(__impl assertion | $($rest)*)
		}
	);

	// Member-mapping.
	(__impl $assertion:ident | . $map_func:ident ( $($args:tt)* ) $($rest:tt)* ) => (
		{
			let temp = $assertion.map_with_stringify(
				|x| x.$map_func($($args)*),
				stringify!(value.$map_func($($args)*))
			);
			new_assert_that!(__impl temp | $($rest)*)
		}
	);
	// If there are no arguments to a mapped function, you can omit the parens.
	(__impl $assertion:ident | . $map_func:ident $($rest:tt)* ) => (
		new_assert_that!(__impl $assertion | . $map_func ( ) $($rest)* )
	);

	// External-mapping.
	(__impl $assertion:ident | => $map_func:ident ( $($args:tt)* ) $($rest:tt)* ) => (
		{
			let temp = $assertion.map_with_stringify(
				|x| $map_func(x, $($args)*),
				stringify!($map_func(value, $($args)*))
			);
			new_assert_that!(__impl $assertion | $($rest)*)
		}
	);
	// If there are no arguments to a mapped function, you can omit the parens.
	(__impl $assertion:ident | => $map_func:ident $($rest:tt)* ) => (
		new_assert_that!(__impl $assertion | $map_func ( ) $($rest)* )
	);

	// Base case.
	(__impl $assertion:ident | ) => (
		$assertion
	)
}

// Usage:
// 
// assert_that!(thing(input)).is(other_thing);
// 
// For using functions on the output of thing, do:
// 
// assert_that!(thing(input) => x, x.is_err()).is(true);
//
// It's the same syntax as a lambda: varaible_name, body
// Functions can be chained:
// 
// assert_that!(thing(input) => x, x.get_name() => x.len()).is(3);
//
//
// For functions that take no parameters, or for fields, you can omit the lambda:
//
// assert_that!(thing(input) => get_name => len).is(3);
//
// You can't mix and match the two, though.
// If your test function only has one parameter, and that parameter implements Clone,
// then you can put it first:
// 
// assert_that!(input, thing => x, x.get_name() => x.len()).is(3);
// assert_that!(input, thing => get_name => len).is(3);
//
// This allows the input to be put into the test message.
#[allow(unused_macros)]
macro_rules! assert_that {
	// The simple cases.
	($subject:expr) => {
		{
			let line = line!().to_string();
			let file = file!().to_string();
			$crate :: assert::Assertion::new($subject).set_location(file, line)
		}
	};
	($input:expr, $test_func:ident) => {
		{
			let line = line!().to_string();
			let file = file!().to_string();
			$crate :: assert::Assertion::new_from_input(&$input, |i| $test_func(&i)).set_location(file, line)
		}
	};

	// Built-in mapping.
	($subject:expr $(=> $x:ident, $map_body:expr)+ ) => {
		{
			let temp = assert_that!($subject);
			assert_that!(__impl_map temp $(=> $x, $map_body)+)
		}
	};
	(__impl_map $subject:ident => $last_x:ident, $last_body:expr) => {
		$subject.map(|$last_x| $last_body)
	};
	(__impl_map $subject:ident => $first_x:ident, $first_body:expr $(=> $rest_x:ident, $rest_body:expr)+ ) => {
		{
			let temp = $subject.map(|$first_x| $first_body);
			assert_that!(__impl_map temp $(=> $rest_x, $rest_body)+)
		}
	};

	// Parameterless mapping.
	($subject:expr $(=> $func:ident)+) => {
		{
			let temp = assert_that!($subject);
			assert_that!(__impl_no_map temp $(=> $func)+)
		}
	};
	(__impl_no_map $subject:ident => $last_func:ident) => {
		$subject.map(|x| x.$last_func())
	};
	(__impl_no_map $subject:ident => $first_func:ident $(=> $rest_func:ident)+ ) => {
		{
			let temp = $subject.map(|x| x.$first_func);
			assert_that!(__impl_no_map temp $(=> $rest_func)+)
		}
	};

	// With test function input.
	($input:expr, $test_func:ident $(=> $x:ident, $b:expr)*) => {
		{
			let temp = assert_that!($input, $test_func);
			assert_that!(__impl_map temp $(=> $x, $b)*)
		}
	};
	($input:expr, $test_func:ident $(=> $func:ident)+) => {
		{
			let temp = assert_that!($input, $test_func);
			assert_that!(__impl_no_map temp $(=> $func)+)
		}
	};
}

// Convenience version of assert_that!() for the .is(true) case.
#[allow(unused_macros)]
macro_rules! assert_is_true {
	($subject:expr $(=> $x:ident, $b:expr)* ) => {
		assert_that!($subject $(=> $x, $b)*).is(true)
	};
	($subject:expr $(=> $f:ident)+ ) => {
		assert_that!($subject $(=> $f)+).is(true)
	};

	($input:expr, $test_func:ident $(=> $x:ident, $b:expr)* ) => {
		assert_that!($input, $test_func $(=> $x, $b)*).is(true)
	};
	($input:expr, $test_func:ident $(=> $f:ident)+ ) => {
		assert_that!($input, $test_func $(=> $f)+ ).is(true)
	}
}

// Convenience version of assert_that!() for the .is(false) case.
#[allow(unused_macros)]
macro_rules! assert_is_false {
	($subject:expr $(=> $x:ident, $b:expr)* ) => {
		assert_that!($subject $(=> $x, $b)*).is(false)
	};
	($subject:expr $(=> $f:ident)+ ) => {
		assert_that!($subject $(=> $f)+).is(false)
	};

	($input:expr, $test_func:ident $(=> $x:ident, $b:expr)* ) => {
		assert_that!($input, $test_func $(=> $x, $b)*).is(false)
	};
	($input:expr, $test_func:ident $(=> $f:ident)+ ) => {
		assert_that!($input, $test_func $(=> $f)+ ).is(false)
	}
}


impl<T> Assertion<T>
	where T: Debug
{
	pub fn new(value: T) -> Assertion<T> {
		Assertion {
			input: None,
			subject: value,
			file: None,
			line: None,
			code: None
		}
	}
	
	pub fn new_from_input<F, U: Debug>(input: &U, test: F) -> Assertion<T>
		where F: FnOnce(&U) -> T
	{
		let input_as_string = format!("{:?}", &input);
		let result = test(&input);
		Assertion {
			input: Some(input_as_string),
			subject: result,
			file: None,
			line: None,
			code: None
		}
	}

	pub fn set_location(mut self, file: String, line: String) -> Self {
		self.file = Some(file);
		self.line = Some(line);
		self
	}

	pub fn set_input_code(mut self, code: &str) -> Self {
		self.code = Some(code.to_string());
		self
	}

	pub fn map<F, U: Debug>(self, f: F) -> MappedAssertion<T, U>
		where F: FnOnce(&T) -> U
	{
		let value = f(&self.subject);
		MappedAssertion::new(self, value)
	}

	pub fn map_with_stringify<F, U: Debug>(self, f: F, code: &str) -> MappedAssertion<T, U>
		where F: FnOnce(&T) -> U
	{
		let value = f(&self.subject);
		MappedAssertion::new_with_stringify(self, value, code)
	}

	fn make_error<U: Debug>(mut self, name: &str, other: &U) -> AssertionFailure {
		let mut error = AssertionFailure::new(name, &self.subject, &other);
		self.file.iter().for_each(|file| error.with_file(&file));
		self.line.iter().for_each(|line| error.with_line(&line));
		self.input.iter().for_each(|input| error.with_input(&input));
		error.with_input_code(self.code.take());
		error
	}
}

impl<T> Assertion<T>
	where T: Debug
{
	pub fn is(self, other: T) {
		if !self.subject.equal(&other) {
			self.make_error("is", &other).fail();
			unreachable!();
		}
	}

	pub fn is_not(self, other: T) {
		if self.subject.equal(&other) {
			self.make_error("is not", &other).fail();
			unreachable!();
		}
	}
}

trait AssertionEqual<T> {
	fn equal(&self, other: &T) -> bool;
}

default impl<T: PartialEq + Debug> AssertionEqual<T> for T {
	fn equal(&self, other: &T) -> bool {
		self == other
	}
}

default impl<T: Debug> AssertionEqual<T> for T {
	fn equal(&self, other: &T) -> bool {
		let self_dbg = format!("{:?}", &self);
		let other_dbg = format!("{:?}", &other);

		self_dbg == other_dbg
	}
}

impl<T, U> MappedAssertion<T, U>
	where T: Debug, U: Debug
{
	pub fn new(original: Assertion<T>, value: U) -> MappedAssertion<T, U> {
		MappedAssertion {
			original: original,
			mapped_value: value,
			code: None,
			chain: Vec::new()
		}
	}

	pub fn new_with_stringify(original: Assertion<T>, value: U, code: &str) -> MappedAssertion<T, U> {
		MappedAssertion {
			original: original,
			mapped_value: value,
			code: Some(code.to_string()),
			chain: Vec::new()
		}
	}

	fn new_with_chain(original: Assertion<T>, value: U, code: Option<String>, chain: Vec<(String, Option<String>)>)
		-> MappedAssertion<T, U>
	{
		MappedAssertion {
			original: original,
			mapped_value: value,
			code: code,
			chain: chain
		}
	}

	fn push_chain(&mut self) {
		let value_str = format!("{:?}", &self.mapped_value);
		let code = self.code.take();
		self.chain.push((value_str, code));
	}

	pub fn map<F, V: Debug>(mut self, f: F) -> MappedAssertion<T, V>
		where F: FnOnce(U) -> V
	{
		self.push_chain();
		let new_value = f(self.mapped_value);
		MappedAssertion::new_with_chain(self.original, new_value, None, self.chain)
	}

	pub fn map_with_stringify<F, V: Debug>(mut self, f: F, code: &str) -> MappedAssertion<T, V>
		where F: FnOnce(U) -> V
	{
		self.push_chain();
		let new_value = f(self.mapped_value);
		let code_str = code.to_string();
		MappedAssertion::new_with_chain(self.original, new_value, Some(code_str), self.chain)
	}

	fn make_error<V: Debug>(mut self, name: &str, other: &V) -> AssertionFailure {
		let mut error = AssertionFailure::new(name, &self.mapped_value, &other);
		self.original.file.iter().for_each(|file| error.with_file(&file));
		self.original.line.iter().for_each(|line| error.with_line(&line));
		self.original.input.iter().for_each(|input| error.with_input(&input));
		error.with_input_code(self.original.code.take());
		error.mapped_from_with_code(&self.original.subject, self.code.take());
		error.with_chain(&mut self.chain);
		error
	}
}

impl<T, U> MappedAssertion<T, U>
	where
		U: Debug,
		T: Debug
{
	pub fn is(self, other: U) {
		if !self.mapped_value.equal(&other) {
			self.make_error("should be equal to", &other).fail();
		}
	}

	pub fn is_not(self, other: U) {
		if self.mapped_value.equal(&other) {
			self.make_error("should not be equal to", &other).fail();
		}
	}
}

impl AssertionFailure {
	pub fn new<T:Debug, U:Debug>(operation_name: &str, actual: &T, expected: &U) -> AssertionFailure {
		AssertionFailure {
			input: None,
			operation: operation_name.to_string(),
			expected: format!("{:?}", &expected),
			actual: format!("{:?}", &actual),
			file: None,
			line: None,
			input_code: None,
			chain: Vec::new()
		}
	}

	pub fn with_file(&mut self, file: &str) {
		self.file = Some(file.to_string());
	}

	pub fn with_line(&mut self, line: &str) {
		self.line = Some(line.to_string());
	}

	pub fn with_input(&mut self, input: &str) {
		self.input = Some(input.to_string());
	}

	pub fn with_input_code(&mut self, code: Option<String>) {
		self.input_code = code;
	}

	pub fn mapped_from<V: Debug>(&mut self, other: V) {
		self.chain.push((format!("{:?}", other), None));
	}

	pub fn mapped_from_with_code<V: Debug>(&mut self, other: V, code: Option<String>) {
		self.chain.push((format!("{:?}", other), code));
	}

	pub fn with_chain(&mut self, mut new_chain: &mut Vec<(String, Option<String>)>) {
		self.chain.append(&mut new_chain);
	}

	pub fn fail(self) {
		let mut message = String::new();
		let assertion_failed = "Assertion failure";
		let location_at = "at";
		let location_in = "in file";
		let expected_msg = "expected";
		let actual_msg = "actual";
		let mapped_from = "which was mapped from";
		let through = "via the code";
		let input_from = "from the test input";

		let all_strs = vec![
			assertion_failed,
			location_in,
			location_at,
			expected_msg,
			actual_msg,
			mapped_from,
			through,
			input_from
		];

		let padding = all_strs.iter()
			.map(|s| s.len())
			.fold(0, |max, this| cmp::max(max, this))
			+ 1;

		message.push_str(
			&format!("\n\n{:>pad$}: expected value {} actual value\n",
				assertion_failed, &self.operation, pad = padding));
		
		match (self.file, self.line) {
			(Some(f), Some(l)) => message.push_str(
				&format!("{:>pad$}: {}:{}\n", location_at, &f, &l, pad = padding)),
			(Some(f), None) => message.push_str(&format!("{:>pad$}: {}\n", location_in, &f, pad = padding)),
			_ => ()
		}
		
		message.push_str(&format!("{:>pad$}: <{}>\n", expected_msg, &self.expected, pad = padding));
		message.push_str(&format!("{:>pad$}: <{}>\n", actual_msg, &self.actual, pad = padding));
		self.chain.iter().rev().for_each(|&(ref s, ref c)| {
			message.push_str(&format!("{:>pad$}: <{}>\n", mapped_from, s, pad = padding));
			if let &Some(ref code) = c {
				message.push_str(&format!("{:>pad$}: {{ {} }}\n", through, code, pad = padding));
			}
		});
		if let Some(i) = self.input {
			message.push_str(&format!("{:>pad$}: <{}>\n", input_from, i, pad = padding));
			if let Some(code) = self.input_code {
				message.push_str(&format!("{:>pad$}: {{ {} }}\n", through, code, pad = padding));
			}
		}

		panic!(message);
	}
}