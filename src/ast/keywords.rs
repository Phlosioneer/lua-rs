


#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum Keyword {
	True,
	False,
	Nil,
	
	And,
	Or,
	Not,

	If,
	Then,
	Else,
	ElseIf,
	End,

	Break,
	Goto,
	
	Do,
	While,
	Until,
	For,
	Repeat,
	In,
	
	Function,
	Local,
	Return,
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum Symbol {
	Plus,					//		+
	Minus,					//		-
	Star,					//		*
	ForwardSlash,			//		/
	Percent,				//		%
	Carot,					//		^
	HashTag,				//		#
	Amp,					//		&
	Tilde,					//		~
	Bar,					//		|
	DoubleLess,				//		<<
	DoubleGreater,			//		>>
	DoubleForward,			//		//
	DoubleEqual,			//		==
	NotEqual,				//		~=
	LessEqual,				//		<=
	GreaterEqual,			//		>=
	Less,					//		<
	Greater,				//		>
	Equal,					//		=
	OpenParen,				//		(
	CloseParen,				//		)
	OpenCurl,				//		{
	CloseCurl,				//		}
	OpenSquare,				//		[
	CloseSquare,			//		]
	DoubleColon,			//		::
	SemiColon,				//		;
	Colon,					//		:
	Comma,					//		,
	Period,					//		.
	DotDot,					//		..
	Ellipsis,				//		...

	// These aren't in the listing (section 3.1 of the manual) but are still special.
	OpenLongBracket(u8),	//		[==[	(see section 3.1)
	CloseLongBracket(u8),	//		]==]	(see section 3.1)
	Comment,				//		--

	// These are used when the level of the LongBracket doesn't fit in a u8.
	OpenTooLongBracket(String),
	CloseTooLongBracket(String),

	// Including these invalid symbols will help make better error messages.
	ErrNotEqual,			//		!=
	ErrNot,					//		!
	ErrAnd,					//		&&
	ErrOr,					//		||
	ErrTripleEqual,			//		===
	ErrMultEqual,			//		*=
	ErrDivEqual,			//		/=
	ErrAddEqual,			//		+=
	ErrSubEqual,			//		-=
	ErrIncrement,			//		++
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum EscapeSequence {
	Bell,					// \a
	Backspace,				// \b
	FormFeed,				// \f
	Newline,				// \n
	Carriage,				// \r
	Tab,					// \t
	VerticalTab,			// \v
	BackSlash,				// \\
	DoubleQuote,			// \"
	SingleQuote,			// \'

	// TODO: No EOF?
	// TODO: No \0?

	// Unusual escapes
	LineBreak,				// "A backslash followed by a line break"
	SkipWhitespace,			// "\z skips the following span of white-space characters, including
							// line breaks"
	
	// Compound escapes
	Byte(u8),
	Unicode(u64),

	// Errors
	InvalidEscape(char),
	ByteTooLarge(u16),
	InvalidDecimalByte(char, char, char),	// Note: The first char is guaranteed to be a digit.
	InvalidHexByte(char, char),
	UnicodeWithoutBrackets(String, MissingBracket),
	UnicodeTooLarge(String),
	InvalidUnicode(String),
	EmptyEscape
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum MissingBracket {
	Start, End, Both
}

impl Symbol {
	pub fn is_open_long_bracket(&self) -> bool {
		use self::Symbol::OpenLongBracket;

		match self {
			&OpenLongBracket(_) => true,
			_ => false
		}
	}

	pub fn get_bracket_depth(&self) -> Option<u8> {
		use self::Symbol::{OpenLongBracket, CloseLongBracket};

		match self {
			&OpenLongBracket(depth) => Option::Some(depth),
			&CloseLongBracket(depth) => Option::Some(depth),
			_ => None
		}
	}
}

#[cfg(test)]
mod test {
	
	

	#[test]
	fn test_symbol_is_open_long_bracket() {
		use super::Symbol::{OpenLongBracket, CloseLongBracket, OpenTooLongBracket, Plus};

		assert_is_true!(OpenLongBracket(5) => is_open_long_bracket);
		assert_is_true!(OpenLongBracket(0) => is_open_long_bracket);

		assert_is_false!(Plus => is_open_long_bracket);
		assert_is_false!(CloseLongBracket(2) => is_open_long_bracket);
		assert_is_false!(OpenTooLongBracket("=".to_string()) => is_open_long_bracket);
	}

	#[test]
	fn test_symbol_get_bracket_depth() {
		use super::Symbol::{OpenLongBracket, CloseLongBracket, OpenTooLongBracket};
		use super::Symbol::{CloseTooLongBracket, Plus};

		assert_that!(OpenLongBracket(5) => get_bracket_depth).is(Some(5));
		assert_that!(OpenLongBracket(0) => get_bracket_depth).is(Some(0));
		assert_that!(CloseLongBracket(5) => get_bracket_depth).is(Some(5));
		assert_that!(CloseLongBracket(0) => get_bracket_depth).is(Some(0));
		
		assert_that!(OpenTooLongBracket("=".to_string()) => get_bracket_depth).is(None);
		assert_that!(CloseTooLongBracket("=".to_string()) => get_bracket_depth).is(None);
		assert_that!(Plus => get_bracket_depth).is(None);
	}
}
