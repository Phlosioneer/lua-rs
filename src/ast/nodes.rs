use std::string::String;
use std::vec::Vec;
use ast::keywords::EscapeSequence;
use itertools::Either;


// This AST breakdown is ripped straight from the Lua docs.

// TODO: What does "stat" mean?
// TODO: WHat does "retstat" mean?
#[derive(Clone, Debug, PartialEq)]
pub struct Block {
	pub statements: Vec<Statement>,
	pub return_statements: Option<Vec<Expression>>
}

pub type Chunk = Block;

pub type Table = Vec<Field>;

#[derive(Clone, Debug, PartialEq)]
pub enum Statement {
	Empty,
	Assign(Variable, Expression),
	FuncCall(FuncCall),
	Label(String),
	Break,
	Goto(String),
	Do(Box<Block>),
	WhileDo(Expression, Box<Block>),
	Repeat(Expression, Box<Block>),
	If(IfStatement),
	For(VanillaForStatement),
	ForIn(ForInStatement),
	FuncDef(Vec<String>, FuncBody),
	LocalFuncDef(String, FuncBody),
	LocalAssign(Vec<String>, Vec<Expression>)
}

// Note: Subexpressions are flattened during parsing.
#[derive(Clone, Debug, PartialEq)]
pub enum Expression {
	Nil,
	Bool(bool),
	Number(Number),
	StringLiteral(StringLiteral),
	Ellipsis,
	Variable(Variable),
	FuncCall(FuncCall),
	Table(Table),
	Binop(BinopExpression),
	Unop(Unop, Box<Expression>),
	
	// Note: This is for an *inline* function defintion. For a general function
	// definition, see Statement::FuncDef and Statement::LocalFuncDef
	FuncDef(FuncBody),
}

#[derive(Clone, Debug, PartialEq)]
pub enum PrefixExpression {
	FuncCall(FuncCall),
	Variable(Variable),
	SubExpression(Box<Expression>)
}

#[derive(Clone, Debug, PartialEq)]
pub enum Variable {
	Simple(String),
	Map(Box<Expression>, Box<Expression>),
	Member(Box<Expression>, String)
}

// Note: Because the function to call and its arguments are determined by an expression,
// it can start with open parentheses. If it does, there's a possibility of ambiguity if
// the previous line doesn't end in a semicolon. Therefore, function calls are parsed
// *greedily*, i.e. the earliest possible match for a function call is taken.
//
// See 3.3.1 for more info.
#[derive(Clone, Debug, PartialEq)]
pub struct FuncCall {
	pub function: Box<Expression>,
	pub args: FuncArgs
}

// Function calls are weird in Lua.
#[derive(Clone, Debug, PartialEq)]
pub enum FuncArgs {
	ArgList(Vec<Expression>),
	ArgTable(Vec<Field>),
	JustAString(String)		// TODO: WTF? How does this work?
}

#[derive(Clone, Debug, PartialEq)]
pub struct FuncBody {
	pub params: Params,
	pub block: Box<Block>
}

#[derive(Clone, Debug, PartialEq)]
pub enum Params {
	Ellipsis,
	Names(Vec<String>),
	NamesAndEllipsis(Vec<String>)
}

#[derive(Clone, Debug, PartialEq)]
pub struct VanillaForStatement {
	pub var_name: String,
	pub assign_expression: Box<Expression>,
	pub predicate: Box<Expression>,
	pub increment_expression: Option<Box<Expression>>,
	pub block: Box<Block>
}

#[derive(Clone, Debug, PartialEq)]
pub struct ForInStatement {
	pub names: Vec<String>,
	pub list: Vec<Expression>,
	pub block: Box<Block>
}

#[derive(Clone, Debug, PartialEq)]
pub struct IfStatement {
	pub predicate: Box<Expression>,
	pub then_block: Box<Block>,
	pub else_block: Option<ElseStatement>,
}

#[derive(Clone, Debug, PartialEq)]
pub enum ElseStatement {
	ChainedIf(Box<IfStatement>), 
	ElseBlock(Box<Block>)
}

#[derive(Clone, Debug, PartialEq)]
pub struct Field {
	key: FieldKey,
	value: Box<Expression>
}

#[derive(Clone, Debug, PartialEq)]
pub enum FieldKey {
	Expression(Box<Expression>),
	Named(String),
	Implicit
}

#[derive(Clone, Debug, PartialEq)]
pub struct BinopExpression {
	first: Box<Expression>,
	op: Binop,
	second: Box<Expression>
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum Binop {
	Add,
	Sub,
	Mult,
	Div,
	FloorDiv,
	Power,
	Mod,
	BitAnd,
	BitOr,
	BitXOr,
	ShiftR,
	ShiftL,
	LogicAnd,
	LogicOr,
	Concat,

	// read as in 5 <name> 6
	// e.g. "5 LessThan 6" becomes "5 < 6", so LessThan is '<'
	LessThan,
	LessThanEqual,
	GreaterThan,
	GreaterThanEqual,
	Equal,
	NotEqual,

}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum Unop {
	Negative,
	LogicNot,
	BitNot,

	// The '#' operator; see lua manual section 3.4.7
	LengthOp,
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum Number {
	Int(i64),
	Float(f64)
}

#[derive(Clone, Debug, Eq)]
pub struct StringLiteral {
	text: Vec<Either<String, EscapeSequence>>,
	is_bracketed: bool,

	// Cached value.
	// TODO: Might be a good place for a Cell<Option<bool>>, for interior
	// mutability.
	is_valid: Option<bool>
}

impl Field {
	pub fn new_bracketed(key: Expression, value: Expression) -> Field {
		Field {
			key: FieldKey::Expression(Box::new(key)),
			value: Box::new(value)
		}
	}

	pub fn new_named(key: &str, value: Expression) -> Field {
		Field {
			key: FieldKey::Named(key.to_string()),
			value: Box::new(value)
		}
	}

	pub fn new_implicit(value: Expression) -> Field {
		Field {
			key: FieldKey::Implicit,
			value: Box::new(value)
		}
	}
}

impl BinopExpression {
	pub fn new(first_expr: Expression, op: Binop, second_expr: Expression) -> BinopExpression {
		BinopExpression {
			first: Box::new(first_expr),
			op: op,
			second: Box::new(second_expr)
		}
	}
}

impl StringLiteral {

	pub fn new(text: Vec<Either<String, EscapeSequence>>) -> StringLiteral {
		let valid = Self::check_is_valid(&text);
		
		StringLiteral {
			text: text,
			is_bracketed: false,
			is_valid: Some(valid)
		}
	}

	pub fn lazy_new(text: Vec<Either<String, EscapeSequence>>) -> StringLiteral {
		StringLiteral {
			text: text,
			is_bracketed: false,
			is_valid: None
		}
	}

	pub fn new_bracketed(text: String) -> StringLiteral {
		StringLiteral {
			text: vec![Either::Left(text)],
			is_bracketed: true,
			is_valid: None
		}
	}

	fn check_is_valid(text: &Vec<Either<String, EscapeSequence>>) -> bool {
		use self::Either::{Left, Right};

		// Check for any newlines or backslashes
		let contains_newline = text.iter().fold(false, |prev, pair| prev || match pair {
			&Right(_) => false,
			// TODO: There's no need to iterate over the string twice...
			&Left(ref s) => s.contains('\n') || s.contains('\\')
		});

		// Consecutive strings aren't allowed. (They would be merged by the parser.)
		let (double_string, _) = text.iter().fold((false, false), |(double_found, was_str), pair| {
			match (double_found, was_str, pair) {
				// Consecutive strings already found.
				(true, _, _) => (true, was_str),

				// This is not a string.
				(false, _, &Right(_)) => (false, false),

				// One string found...
				(false, false, &Left(_)) => (false, true),

				// ...two strings found.
				(false, true, &Left(_)) => (true, true)
			}
		});

		let invalid = contains_newline || double_string;
		return !invalid;
	}

	pub fn get_text(&self) -> &Vec<Either<String, EscapeSequence>> {
		&self.text
	}

	pub fn get_is_valid(&self) -> bool {
		if self.is_bracketed {
			return true;
		}

		match self.is_valid {
			Some(ret) => ret,
			None => Self::check_is_valid(&self.text)
		}
	}

	pub fn get_is_valid_cached(&mut self) -> bool {
		if self.is_bracketed {
			return true;
		}

		let ret = self.get_is_valid();
		if self.is_valid == None {
			self.is_valid = Some(ret);
		}
		return ret;
	}

	pub fn get_is_valid_panic(&self) -> bool {
		if self.is_bracketed {
			return true;
		}

		self.is_valid.unwrap()
	}
}

impl PartialEq for StringLiteral {
	fn eq(&self, other: &Self) -> bool {
		// is_valid is a cached value, so just compare the actual data.
		// TODO: Copy the cached value between them, if I make is_valid a Cell.
		(self.text == other.text) && (self.is_bracketed == other.is_bracketed)
	}

	fn ne(&self, other: &Self) -> bool {
		!self.eq(other)
	}
}

#[cfg(test)]
mod test {

	use super::StringLiteral;
	use super::Either;
	use super::Either::{Left, Right};
	use super::EscapeSequence;
	

	#[test]
	fn test_string_literal() {
		let valid_test_array = vec![
			Left("Hello,".to_string()),
			Right(EscapeSequence::Newline),
			Left("World!".to_string()),
			Right(EscapeSequence::InvalidEscape(' '))
		];

		let newline_test_array = vec![
			Right(EscapeSequence::SkipWhitespace),
			Left("This string has a \n newline".to_string())
		];

		let double_string_test_array = vec![
			Right(EscapeSequence::VerticalTab),
			Left("This is a red herring;".to_string()),
			Right(EscapeSequence::Bell),
			Right(EscapeSequence::Bell),
			Left("*this* is really".to_string()),
			Left("the double string!".to_string())
		];

		let backslash_test_array = vec![
			Left("Straight to the \\ point.".to_string())
		];
		
		let empty_test_array = vec![];

		let mut newline_and_double_test = newline_test_array.clone();
		newline_and_double_test.append(&mut double_string_test_array.clone());

		let mut backslash_and_newline_test = backslash_test_array.clone();
		backslash_and_newline_test.append(&mut newline_test_array.clone());

		let mut double_and_backslash_test = double_string_test_array.clone();
		double_and_backslash_test.append(&mut backslash_test_array.clone());

		let mut really_long_valid_array = Vec::new();
		for _ in 0..10 {
			really_long_valid_array.append(&mut valid_test_array.clone());
		}

		test_string_literal_with_input(valid_test_array, true);
		test_string_literal_with_input(really_long_valid_array, true);
		test_string_literal_with_input(empty_test_array, true);

		test_string_literal_with_input(newline_test_array, false);
		test_string_literal_with_input(double_string_test_array, false);
		test_string_literal_with_input(backslash_test_array, false);
		test_string_literal_with_input(newline_and_double_test, false);
		test_string_literal_with_input(backslash_and_newline_test, false);
		test_string_literal_with_input(double_and_backslash_test, false);
	}

	fn test_string_literal_with_input(input: Vec<Either<String, EscapeSequence>>, valid: bool) {
		println!("Current test array: {:?}\nShould be valid: {}", &input, valid);

		let test1 = StringLiteral::new(input.clone());
		assert_that!(&test1 => get_is_valid_panic).is(valid);
		assert_that!(&test1 => get_is_valid).is(valid);

		let test2 = StringLiteral::new(input.clone());
		assert_that!(&test1).is(&test2);

		let mut test3 = StringLiteral::lazy_new(input);
		assert_that!(&test3 => get_is_valid).is(valid);
		assert_that!(&test1).is(&test3);

		// Test the caching.
		assert_that!(test3.get_is_valid_cached()).is(valid);
		assert_that!(&test3 => get_is_valid_panic).is(valid);
		assert_that!(&test3 => get_is_valid).is(valid);
	}

	#[test]
	#[should_panic]
	fn test_string_literal_cache_panic() {
		let test_vec = vec![Left("hi!".to_string()), Right(EscapeSequence::Tab)];
		let test = StringLiteral::lazy_new(test_vec);
		test.get_is_valid_panic();
	}

	#[test]
	fn test_string_literal_bracketed() {

		let test1 = do_string_literal_bracket_test("Hello!".to_string());
		let test2 = do_string_literal_bracket_test("Illegal \n stuff.".to_string());
		do_string_literal_bracket_test("Escapes! \\a Even unpaired stuff: \\".to_string());

		assert_that!(test1).is_not(test2);
		
	}

	fn do_string_literal_bracket_test(s: String) -> StringLiteral {
		

		let test_vec = vec![Left(s.clone())];
		let test_unbracketed = StringLiteral::new(test_vec.clone());
		let mut test_bracketed1 = StringLiteral::new_bracketed(s.clone());
		let test_bracketed2 = StringLiteral::new_bracketed(s);
		
		assert_that!(&test_bracketed1).is(&test_bracketed2);
		assert_that!(&test_unbracketed).is_not(&test_bracketed1);
		assert_is_true!(&test_bracketed1 => get_is_valid_panic);
		assert_is_true!(&test_bracketed1 => get_is_valid);
		assert_is_true!(test_bracketed1.get_is_valid_cached());

		// Return this for further testing.
		test_bracketed1
	}
}